package com.ripley.commerce.dispatch.service;

import java.util.List;

import com.ripley.commerce.dispatch.dao.CheckDispatchBTDao;
import com.ripley.commerce.dispatch.dao.CheckDispatchMEDao;
import com.ripley.commerce.dispatch.dto.ArticuloVenta;
import com.ripley.commerce.dispatch.dto.Despacho;
import com.ripley.commerce.dispatch.dto.NotaVenta;
import com.ripley.commerce.dispatch.config.ApplicationParameters;
import com.ripley.commerce.dispatch.rest.ProductViewRest;
import com.ripley.commerce.dispatch.silverpop.TransactXML;

public class CheckDispatchServiceImpl implements CheckDispatchService {

    private CheckDispatchMEDao checkDispatchMEDao;

    private CheckDispatchBTDao checkDispatchBTDao;

    private ProductViewRest productViewRest;

    private TransactXML transactXML;

    @Override
    public String findParameter(String parameterName, String applicationID) {
        return this.checkDispatchMEDao.findParameter(parameterName, applicationID);
    }

    @Override
    public String findQuery(String queryID) {
        return this.checkDispatchMEDao.findQuery(queryID);
    }

    @Override
    public List<NotaVenta> findAllOrderItems(String initializationQuery, String wildCard, int jobID, int initRange, int endRange, int rowsLimit) {
        return checkDispatchMEDao.findAllOrderItems(initializationQuery, wildCard, jobID, initRange, endRange, rowsLimit);
    }
    
    @Override
    public List<NotaVenta> actualizarArticulos(List<NotaVenta> lista){
        return checkDispatchMEDao.actualizarArticulos(lista);
    }

    @Override
    public List<NotaVenta> findOrdersByOrdersId(ApplicationParameters parameters) {
        return checkDispatchMEDao.findOrdersByOrdersId(parameters);
    }

    @Deprecated
    @Override
    public List<NotaVenta> findAllOrderItems(ApplicationParameters parameters) {
        return checkDispatchMEDao.findAllOrderItems(parameters);
    }

    @Override
    public List<NotaVenta> findOrderItemsByOrderIds(ApplicationParameters parameters) {
        return checkDispatchMEDao.findOrderItemsByOrderIds(parameters);
    }

    @Override
    public void editItem(String correlativoVenta, ArticuloVenta articuloVenta) {
        checkDispatchMEDao.editItem(correlativoVenta, articuloVenta);
    }

    @Override
    public String findThumbnailImageItem(String orderId) {
        return productViewRest.findThumbnailImageItem(orderId);
    }

    public @Override
    String sendEmail(String request) {
        return transactXML.sendEmail(request);
    }

    @Override
    public String getXMLWithDataRetiro(NotaVenta notaVenta) {
        return transactXML.getXMLWithDataRetiro(notaVenta);
    }

    @Override
    public String getXMLWithDataNOK(NotaVenta notaVenta) {
        return transactXML.getXMLWithDataNOK(notaVenta);
    }

    @Override
    public List<Despacho> findDispatchStatusByCUDs(String cuds) {
        return checkDispatchBTDao.findDispatchStatusByCUDs(cuds);
    }

    public void setCheckDispatchMEDao(CheckDispatchMEDao checkDispatchMEDao) {
        this.checkDispatchMEDao = checkDispatchMEDao;
    }

    public void setCheckDispatchBTDao(CheckDispatchBTDao checkDispatchBTDao) {
        this.checkDispatchBTDao = checkDispatchBTDao;
    }

    public void setProductViewRest(ProductViewRest productViewRest) {
        this.productViewRest = productViewRest;
    }

    public void setTransactXML(TransactXML transactXML) {
        this.transactXML = transactXML;
    }

    @Override
    public void updateSendEmailState(String correlativoVenta, int estadoEnvioCorreo, int tipoCorreo) {
        this.checkDispatchMEDao.updateSendEmailState(correlativoVenta, estadoEnvioCorreo, tipoCorreo);
    }

    @Override
    public void updateCUDState(String correlativoVenta, int estado) {
        this.checkDispatchMEDao.updateCUDState(correlativoVenta, estado);
    }
    
    @Override
    public void updateCUDState1(String correlativoVenta, int estado) {
        this.checkDispatchMEDao.updateCUDState1(correlativoVenta, estado);
    }
    
    @Override
    public void updateCUDState2(String correlativoVenta, int estado) {
        this.checkDispatchMEDao.updateCUDState2(correlativoVenta, estado);
    }
    
    @Override
    public void updateNCState(int correlativoVenta, String codDespacho, int nuevoEstado, int estadoAModificar) {
        this.checkDispatchMEDao.updateNCState(correlativoVenta, codDespacho, nuevoEstado, estadoAModificar);
    }
    
    @Override
    public void actualizarRegistrosEnTablaEstadoSincronizacionBT(String query, int rows, int initRange){
        this.checkDispatchMEDao.actualizarRegistrosEnTablaEstadoSincronizacionBT(query, rows, initRange);
    }
}
