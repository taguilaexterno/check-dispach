package com.ripley.commerce.dispatch.service;

import java.util.List;

import com.ripley.commerce.dispatch.dto.ArticuloVenta;
import com.ripley.commerce.dispatch.dto.Despacho;
import com.ripley.commerce.dispatch.dto.NotaVenta;
import com.ripley.commerce.dispatch.config.ApplicationParameters;

public interface CheckDispatchService {

    public String findQuery(String queryID);
    
    public String findParameter(String parameterName, String applicationID);

    public List<NotaVenta> findAllOrderItems(String initializationQuery, String wildCard, int jobID, int initRange, int endRange, int rowsLimit);

    public List<NotaVenta> actualizarArticulos(List<NotaVenta> lista);
    
    @Deprecated
    public List<NotaVenta> findAllOrderItems(ApplicationParameters parameters);

    public List<NotaVenta> findOrderItemsByOrderIds(ApplicationParameters parameters);

    public List<NotaVenta> findOrdersByOrdersId(ApplicationParameters parameters);

    public void editItem(String correlativoVenta, ArticuloVenta articuloVenta);

    public String findThumbnailImageItem(String orderId);

    public String sendEmail(String request);

    public String getXMLWithDataRetiro(NotaVenta notaVenta);

    public String getXMLWithDataNOK(NotaVenta notaVenta);

    public List<Despacho> findDispatchStatusByCUDs(String cuds);

    public void updateSendEmailState(String correlativoVenta, int estadoEnvioCorreo, int tipoCorreo);

    public void updateCUDState(String correlativoVenta, int estado);
    public void updateCUDState1(String correlativoVenta, int estado);
    public void updateCUDState2(String correlativoVenta, int estado);
    
    public void updateNCState(int corrrelativoVenta, String codDespacho, int nuevoEstado, int estadoAModificar);
    
    public void actualizarRegistrosEnTablaEstadoSincronizacionBT(String query, int rows, int initRange);
}
