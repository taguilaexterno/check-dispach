package com.ripley.commerce.dispatch.rest;

public interface ProductViewRest {

	public String findThumbnailImageItem(String orderId);
}
