package com.ripley.commerce.dispatch.rest;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;

import com.ripley.commerce.dispatch.util.Constants;
import com.ripley.commerce.dispatch.util.Utils;

public class ProductViewRestImpl implements ProductViewRest {

	private static Log log = LogFactory.getLog(ProductViewRestImpl.class);
	
	private String apiURLProductView;
	
	public void setApiURLProductView(String apiURLProductView) {
		this.apiURLProductView = apiURLProductView;
	}

	public String findThumbnailImageItem(String orderId){
		String urlWithOrder = apiURLProductView.concat(orderId);
//		log.debug("URL ProductView: " + urlWithOrder);
		Map<String, String> requestProperty = new HashMap<String, String>();
		String response = Utils.invokeURL(urlWithOrder, Constants.HTTP_METHOD_GET, null, requestProperty);
		return this.jsonStringToGet(response);
	}
	
	private String jsonStringToGet(String jsonString){
		String urlImage = "";
		try{
			if(jsonString != null && !"".equals(jsonString)){
				JSONObject json = new JSONObject(jsonString);
				JSONArray jsonArray = json.getJSONArray("CatalogEntryView");
				urlImage = jsonArray.getJSONObject(0).getString("thumbnail");
			}
		}catch(Exception ex){
			log.warn(ex.getMessage());
		}
		return urlImage;
	}
}
