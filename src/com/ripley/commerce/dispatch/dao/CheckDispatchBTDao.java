package com.ripley.commerce.dispatch.dao;

import java.util.List;

import com.ripley.commerce.dispatch.dto.Despacho;

public interface CheckDispatchBTDao {
	
	public List<Despacho> findDispatchStatusByCUDs(String cuds);

}
