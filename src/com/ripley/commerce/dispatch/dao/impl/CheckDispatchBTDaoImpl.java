package com.ripley.commerce.dispatch.dao.impl;

import com.ripley.commerce.dispatch.dao.CheckDispatchBTDao;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.ripley.commerce.dispatch.dto.Despacho;
import com.ripley.commerce.dispatch.util.Utils;

public class CheckDispatchBTDaoImpl implements CheckDispatchBTDao {

//	private static Log log = LogFactory.getLog(CheckDispatchMEDaoImpl.class);
    /*
    private static final String SQL_SELECT_BIGT_DESPACHOS_BY_CUDS = "SELECT CUD, COD_MOTIVO, ESTADO_DESPACHO, JORNADA_DESP "
            + "FROM BIGT_DESPACHOS "
            + "WHERE CUD IN (<CUDS>) ";
     */
    private static final String SQL_SELECT_BIGT_DESPACHOS_BY_CUDS = "SELECT "
            + "RES.CUD AS RES_CUD, "
            + "RES.NRO_DCTO AS RES_NRO_DCTO, "
            + "RES.COD_MOTIVO AS RES_COD_MOTIVO, "
            + "RES.JORNADA_DESP AS RES_JORNADA_DESP, "
            + "MOT.DESCRIPCION_MOTIVO AS MOT_DESCRIPCION_MOTIVO, "
            + "DES.CUD AS DES_CUD, "
            + "DES.COD_MOTIVO AS DES_COD_MOTIVO, "
            + "DES.ESTADO_DESPACHO AS DES_ESTADO_DESPACHO, "
            + "DES.JORNADA_DESP AS DES_JORNADA_DESP "
            + "FROM BIGT_RESERVAS RES "
            + "INNER JOIN BIGT_MOTIVO MOT ON MOT.COD_MOTIVO = RES.COD_MOTIVO "
            + "LEFT JOIN  BIGT_DESPACHOS DES ON RES.CUD = DES.CUD "
            + "WHERE MOT.ESTADO_DESPACHO = 'R' "
            + "AND RES.CUD IN (<CUDS>)";

    private JdbcTemplate dataSourceBigTicket;

    @Autowired
    public void setDataSourceBigTicket(DataSource dataSourceBigTicket) {
        this.dataSourceBigTicket = new JdbcTemplate(dataSourceBigTicket);
    }

    public List<Despacho> findDispatchStatusByCUDs(String cuds) {
        String sqlWithWildcards = prepareSqlStatement(SQL_SELECT_BIGT_DESPACHOS_BY_CUDS, cuds);
//		log.debug("SQL: " + sqlWithWildcards);
        Object[] obj = Utils.getArrayFromCommasString(cuds);
        SqlRowSet srs = dataSourceBigTicket.queryForRowSet(sqlWithWildcards, obj);

        List<Despacho> listaCUDs = new ArrayList<Despacho>();

        Despacho despacho = null;
        while (srs.next()) {
            despacho = new Despacho();

            // Datos de bigt_reservas
            despacho.setReservaCud(srs.getString("RES_CUD"));
            despacho.setReservaCodMotivo(srs.getString("RES_COD_MOTIVO"));
            despacho.setReservaJornadaDesp(srs.getString("RES_JORNADA_DESP"));
            // Datos de bigt_MOTIVO
            despacho.setDescripcionMotivo(srs.getString("MOT_DESCRIPCION_MOTIVO"));

            // Datos de bigt_despachos
            despacho.setCud(srs.getString("DES_CUD"));
            despacho.setCodMotivo(srs.getString("DES_COD_MOTIVO"));
            despacho.setEstadoDespacho(srs.getString("DES_ESTADO_DESPACHO"));
            despacho.setJornadaDesp(srs.getString("DES_JORNADA_DESP"));

            listaCUDs.add(despacho);
        }
        return listaCUDs;
    }

    private String prepareSqlStatement(String strSql, String ocs) {
        String wildcards = Utils.generateWildcards(ocs.split(",").length);
        strSql = strSql.replaceAll("<CUDS>", wildcards);
        return strSql;
    }
}
