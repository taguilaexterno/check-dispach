package com.ripley.commerce.dispatch.dao.impl;

import com.google.common.io.CharStreams;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.ripley.commerce.dispatch.dto.ArticuloVenta;
import com.ripley.commerce.dispatch.dto.NotaVenta;
import com.ripley.commerce.dispatch.config.ApplicationParameters;
import com.ripley.commerce.dispatch.dao.CheckDispatchMEDao;
import com.ripley.commerce.dispatch.util.Utils;
import java.io.Reader;
import java.sql.Clob;

public class CheckDispatchMEDaoImpl implements CheckDispatchMEDao {

    private static Log log = LogFactory.getLog(CheckDispatchMEDaoImpl.class);

    private static final int TIPO_CORREO_LISTO_PARA_RETIRAR = 1;
    private static final int TIPO_CORREO_EXCEPCION = 2;

    private final static String SQL_SELECT_ARTICULO_VENTA_RTST_BOLETA = "SELECT ROWNUM, "
            + "       ARTV.* "
            + "FROM "
            + "  (SELECT AV.CORRELATIVO_VENTA, "
            + "          AV.COD_ARTICULO, "
            + "          AV.DESC_RIPLEY, "
            + "          AV.UNIDADES, "
            + "          AV.PRECIO, "
            + "          AV.COD_DESPACHO, "
            + "          AV.TIPO_DESPACHO, "
            + "          AV.ESTADO_VENTA, "
            + "          AV.FECHA_DESPACHO, "
            + "          AV.COD_BODEGA, "
            + "          AV.TIPO_PAPEL_REGALO "
            + "   FROM ARTICULO_VENTA AV "
            + "   JOIN NOTA_VENTA NV ON NV.CORRELATIVO_VENTA = AV.CORRELATIVO_VENTA "
            + "   WHERE NV.ESTADO = 2 "
            + "     <INIT_DATE> "
            + "     AND (AV.TIPO_DESPACHO = 'ST' OR AV.TIPO_DESPACHO ='RT') "
            + "     AND AV.ESTADO_VENTA NOT IN ('COSTO DE ENVIO', "
            + "                                 'Producto Retirado', "
            + "                                 'Correo Enviado', "
            + "                                 'Sin Stock', "
            + "                                 'Reserva Cancelada', "
            + "                                 'Reserva Caducada', "
            + "                                 'Devuelto', "
            + "                                 'Reembolsado', "
            + "                                 'Inconveniente en la orden', "
            + "                                 'Jornada distinta en BT') "
            + "     <DATE_DISPATCH> "
            + "   ORDER BY AV.CORRELATIVO_VENTA, AV.FECHA_DESPACHO ASC) ARTV "
            + "WHERE ROWNUM <= ? ";

    private final static String SQL_SELECT_LISTO_PARA_RETIRAR = "SELECT "
            + "N.CORRELATIVO_VENTA, "
            + "D.EMAIL_CLIENTE, "
            + "D.DIRECCION_DESPACHO, "
            + "D.FECHA_DESPACHO, "
            + "D.NOMBRE_DESPACHO, "
            + "'RET_TIENDA' AS TIPO_DESPACHO, "
            + "D.COMUNA_DESPACHO_DES, "
            + "D.REGION_DESPACHO_DES "
            + "FROM NOTA_VENTA N, DESPACHO D "
            + "WHERE N.CORRELATIVO_VENTA = D.CORRELATIVO_VENTA  "
            + "AND N.CORRELATIVO_VENTA = D.CORRELATIVO_VENTA "
            + "<OCS> ";

    private String strSql;

    private static final String SQL_UPDATE_ARTICULO_VENTA_ESTADO_VENTA = "UPDATE ARTICULO_VENTA SET TIPO_DESPACHO = ?, ESTADO_VENTA = ?, ES_NC = ?  WHERE COD_DESPACHO = ? AND CORRELATIVO_VENTA = ?";

    private JdbcTemplate dataSourceModeloExtendido;

    @Autowired
    public void setDataSourceModeloExtendido(DataSource dataSourceModeloExtendido) {
        this.dataSourceModeloExtendido = new JdbcTemplate(dataSourceModeloExtendido);
    }

    @Override
    public String findParameter(String parameterName, String applicationID) {
        String query = "SELECT VALUE FROM JEN_PARAMETERS WHERE APPLICATION = ? AND NAME = ?";
        SqlRowSet srs = null;
        String value = null;
        try {
            srs = dataSourceModeloExtendido.queryForRowSet(query, new Object[]{applicationID, parameterName});
            if (srs != null) {
                while (srs.next()) {
                    value = srs.getString("VALUE");
                    break;
                }
            }
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        } finally {
            query = null;
            srs = null;
        }
        return value;
    }

    @Override
    public String findQuery(String idQuery) {
        // TODO: definir CHECKTRACKING_SIMPLE como parametro de entrada
        String sql = "SELECT BIG_VALUE FROM JEN_PARAMETERS WHERE APPLICATION = 'CHECKTRACKING_SIMPLE' AND NAME = ?";
        String result = null;
        log.debug("SQL: " + sql);
        SqlRowSet srs = dataSourceModeloExtendido.queryForRowSet(sql, new Object[]{idQuery});
        if (srs != null) {
            while (srs.next()) {
                try {
                    Clob clobObject = ((Clob) srs.getObject("BIG_VALUE"));

                    Reader initialReader = clobObject.getCharacterStream();
                    result = CharStreams.toString(initialReader);
                    initialReader.close();
                    break;
                } catch (Exception ex) {
                    throw new RuntimeException(ex);
                }
            }
        }
        return result;
    }

    @Override
    public List<NotaVenta> findAllOrderItems(String query, String wildCard,
            int jobID, int initRange, int endRange, int rowsLimit) {
        log.debug("SQL: " + query);
        Object[] objArgs = new Object[]{wildCard, jobID, initRange, endRange, rowsLimit};

        SqlRowSet srs = dataSourceModeloExtendido.queryForRowSet(query, objArgs);
        return getListOrderItems(srs);
    }
    
    @Override
    public List<NotaVenta> actualizarArticulos(List<NotaVenta> listaEntrada){
        ArrayList listaArticulos = new ArrayList();
        List<NotaVenta> listaNotaVenta = new ArrayList();
        NotaVenta notaVenta = new NotaVenta();
        SqlRowSet srs = null;
        String select = "SELECT av.* FROM ARTICULO_VENTA av WHERE av.correlativo_venta = ? AND av.cod_despacho = ?";
        for (int i = 0; i < listaEntrada.size(); i++) {
            List<ArticuloVenta> listaArticuloVenta = new ArrayList();
            listaNotaVenta.add(new NotaVenta());
            listaNotaVenta.get(i).setArticulos(listaArticuloVenta);
            for(int j = 0; j < listaEntrada.get(i).getArticulos().size(); j++){
                Object[] objArgs = new Object[]{listaEntrada.get(i).getCorrelativoVenta(), listaEntrada.get(i).getArticulos().get(j).getCodDespacho()};
                srs = dataSourceModeloExtendido.queryForRowSet(select, objArgs);
                listaNotaVenta.get(i).getArticulos().add(getArticuloVenta(srs));
            }
        }
        return listaNotaVenta;
    }

    @Deprecated
    @Override
    public List<NotaVenta> findAllOrderItems(ApplicationParameters parameters) {
        prepareSqlStatement(SQL_SELECT_ARTICULO_VENTA_RTST_BOLETA, parameters);
        log.debug("SQL: " + strSql);
        SqlRowSet srs = dataSourceModeloExtendido.queryForRowSet(strSql, new Object[]{parameters.getRows()});
        return this.getListOrderItems(srs);
    }

    @Deprecated
    @Override
    public List<NotaVenta> findOrderItemsByOrderIds(ApplicationParameters parameters) {
        Object[] objArgs = prepareSqlStatement(SQL_SELECT_ARTICULO_VENTA_RTST_BOLETA, parameters);
        log.debug("SQL: " + strSql);
        SqlRowSet srs = dataSourceModeloExtendido.queryForRowSet(strSql, objArgs);
        return this.getListOrderItems(srs);
    }

    private List<NotaVenta> getListOrderItems(SqlRowSet srs) {
        List<NotaVenta> listaNotaVenta = new ArrayList<>();
        List<ArticuloVenta> listaArticuloVenta = null;
        NotaVenta notaVenta = null;

        if (srs != null) {
            while (srs.next()) {
                if (notaVenta != null && !"".equals(notaVenta.getCorrelativoVenta())
                        && !srs.getString("CORRELATIVO_VENTA").equals(notaVenta.getCorrelativoVenta())) {
                    listaNotaVenta.add(notaVenta);
                }

                if (notaVenta == null || !srs.getString("CORRELATIVO_VENTA").equals(notaVenta.getCorrelativoVenta())) {
                    notaVenta = new NotaVenta();
                    notaVenta.setCorrelativoVenta(srs.getString("CORRELATIVO_VENTA"));
                    notaVenta.setCodBodega(srs.getString("COD_BODEGA"));
                    notaVenta.setFechaBoleta(srs.getString("FECHA_BOLETA"));
                    notaVenta.setHoraBoleta(srs.getString("HORA_BOLETA"));
                    listaArticuloVenta = new ArrayList<>();
                }
                ArticuloVenta articulo = new ArticuloVenta();
                articulo.setCodArticulo(srs.getString("COD_ARTICULO"));
                articulo.setDescRipley(srs.getString("DESC_RIPLEY"));
                articulo.setUnidades(srs.getString("UNIDADES"));
                articulo.setPrecio(srs.getString("PRECIO"));
                articulo.setCodDespacho(srs.getString("COD_DESPACHO"));
                articulo.setTipoDespacho(srs.getString("TIPO_DESPACHO"));
                articulo.setEstadoVenta(srs.getString("ESTADO_VENTA"));
                articulo.setFechaDespacho(srs.getString("FECHA_DESPACHO"));
                articulo.setImagen(srs.getString("TIPO_PAPEL_REGALO"));
                articulo.setEs_nc(srs.getString("ES_NC"));
                listaArticuloVenta.add(articulo);
                notaVenta.setArticulos(listaArticuloVenta);
            }
        }
        if (notaVenta != null) {
            listaNotaVenta.add(notaVenta);
        }

        return listaNotaVenta;
    }

    @Override
    public List<NotaVenta> findOrdersByOrdersId(ApplicationParameters parameters) {
        Object[] objArgs = prepareSqlStatement(SQL_SELECT_LISTO_PARA_RETIRAR, parameters);
        log.debug("SQL: " + strSql);
        SqlRowSet srs = dataSourceModeloExtendido.queryForRowSet(strSql, objArgs);

        List<NotaVenta> listaNotaVenta = new ArrayList<NotaVenta>();
        NotaVenta notaVenta = null;

        while (srs.next()) {
            notaVenta = new NotaVenta();
            notaVenta.setCorrelativoVenta(srs.getString("CORRELATIVO_VENTA"));
            notaVenta.setEmailCliente(srs.getString("EMAIL_CLIENTE"));
            notaVenta.setNombreTienda(srs.getString("DIRECCION_DESPACHO"));
            notaVenta.setFechaDespacho(srs.getString("FECHA_DESPACHO"));
            notaVenta.setNombreDespacho(srs.getString("NOMBRE_DESPACHO"));
            notaVenta.setTipoDespacho(srs.getString("TIPO_DESPACHO"));
            notaVenta.setComunaDespacho(srs.getString("COMUNA_DESPACHO_DES"));
            notaVenta.setRegionDespacho(srs.getString("REGION_DESPACHO_DES"));

            listaNotaVenta.add(notaVenta);
        }
        return listaNotaVenta;
    }

    @Override
    public void editItem(String correlativoVenta, ArticuloVenta articuloVenta) {
        dataSourceModeloExtendido.update(SQL_UPDATE_ARTICULO_VENTA_ESTADO_VENTA, new Object[]{articuloVenta.getTipoDespacho(), articuloVenta.getNuevoEstadoVenta(), articuloVenta.getEs_nc(), articuloVenta.getCodDespacho(), correlativoVenta});
    }

    private Object[] prepareSqlStatement(String strSql, ApplicationParameters parameters) {
        Object[] objArgs = null;

        //Buscar por OCS
        if (parameters.getOrderId() != null && !"".equals(parameters.getOrderId())) {
            objArgs = Utils.getArrayFromCommasString(parameters.getOrderId());
            String ocs = parameters.getOrderId();
            String wildcards = Utils.generateWildcards(ocs.split(",").length);

            strSql = strSql.replaceAll("<OCS>", " AND N.CORRELATIVO_VENTA IN (<OCS>) ");
            //Se agregan tantos ? por cada OC en el SQL
            strSql = strSql.replaceAll("<OCS>", wildcards);
            //Si la consulta es por ordenes, se quita el filtro de la fecha
            strSql = strSql.replaceAll("<INIT_DATE>", "");
            strSql = strSql.replaceAll("<DATE_DISPATCH>", "");
        } else {
            strSql = strSql.replaceAll("<DATE_DISPATCH>", "AND TRUNC(AV.FECHA_DESPACHO) BETWEEN TRUNC(SYSDATE " + parameters.getInitDateDispatch() + ") AND TRUNC(SYSDATE " + parameters.getEndDateDispatch() + ") ");
            //Si la consulta es masiva, se agrega el filtro de la fecha
            strSql = strSql.replaceAll("<INIT_DATE>", "AND TRUNC(NV.FECHA_HORA_CREACION) >= TO_DATE('" + parameters.getInitDate() + "', 'yyyy-mm-dd')");
            //Si la consulta es masiva, se quita el filtro de ordenes
            strSql = strSql.replaceAll("<OCS>", "");
        }
        //SQL FINAL
        this.strSql = strSql;
        return objArgs;
    }

    @Override
    public void updateSendEmailState(String correlativoVenta, int estadoEnvioCorreo, int tipoCorreo) {
        String sql = "UPDATE ESTADO_SINCR_BT SET ";
        if (tipoCorreo == TIPO_CORREO_LISTO_PARA_RETIRAR) {
            sql = sql + "ESTADO_CORREO_LPR = ? ";
        } else if (tipoCorreo == TIPO_CORREO_EXCEPCION) {
            sql = sql + "ESTADO_CORREO_UPS = ? ";
        }
        sql = sql + "WHERE CORRELATIVO_VENTA = ?";
        Object[] objArgs = new Object[]{estadoEnvioCorreo, correlativoVenta};
        dataSourceModeloExtendido.update(sql, objArgs);

    }

    @Override
    public void updateCUDState(String correlativoVenta, int estado) {
        String sql = "UPDATE ESTADO_SINCR_BT SET ESTADO_CUD = ?, FECHA_ULTIMO_PROC = SYSDATE WHERE CORRELATIVO_VENTA = ?";
        Object[] objArgs = new Object[]{estado, correlativoVenta};
        dataSourceModeloExtendido.update(sql, objArgs);
    }
    
    @Override
    public void updateCUDState1(String correlativoVenta, int estado) {
        String sql = "update estado_sincr_bt bt "
                + "set bt.estado_cud = 1, bt.FECHA_ULTIMO_PROC = sysdate "
                + "where bt.correlativo_venta =? "
                + "and not exists("
                + "select a.cod_despacho "
                + "from articulo_venta a "
                + "where a.correlativo_venta = bt.correlativo_venta "
                + "and a.COD_DESAPCHO <> ' ' "
                + "and a.TIPO_DESPACHO = 'RT' "
                + "and UPPER(a.estado_venta) not in ('SIN STOCK','CUD NO EXISTE EN BT','RESERVA CADUCADA','RESERVA CANCELADA','LISTO PARA RETIRAR') "
                + ")";
        Object[] objArgs = new Object[]{correlativoVenta}; //modificado20191107
        dataSourceModeloExtendido.update(sql, objArgs);
    }
    
    @Override
    public void updateCUDState2(String correlativoVenta, int estado) {
        String sql = "update estado_sincr_bt bt set bt.estado_cud = 2, bt.fecha_ultimo_proc = sysdate where bt.correlativo_venta = ? " +
            "and not exists(select a.cod_despacho " +
            "from articulo_venta a " +
            "where a.correlativo_venta = bt.correlativo_venta " +
            "and a.COD_DESPACHO <> ' ' " +
            "and UPPER(a.estado_venta) not in ('SIN STOCK','CUD NO EXISTE EN BT','RESERVA CADUCADA','RESERVA CANCELADA','LISTO PARA RETIRAR')" +
            ") ";
        Object[] objArgs = new Object[]{correlativoVenta}; //modificado20191107
        dataSourceModeloExtendido.update(sql, objArgs);
    }
    
    @Override
    public void updateNCState(int correlativoVenta, String codDespacho, int nuevoEstado, int estadoAModificar) {
        String sql = "UPDATE ARTICULO_VENTA SET ES_NC = ? WHERE CORRELATIVO_VENTA = ? AND COD_DESPACHO = ? AND ES_NC = ?";
        Object[] objArgs = new Object[]{nuevoEstado, correlativoVenta, codDespacho, estadoAModificar};
        dataSourceModeloExtendido.update(sql, objArgs);
    }
    
    @Override
    public void actualizarRegistrosEnTablaEstadoSincronizacionBT(String query, int rows, int initRange){
        log.debug("SQL: " + query);
        Object[] objArgs = null;
        try {
            objArgs = new Object[]{initRange, initRange, initRange, rows};
            dataSourceModeloExtendido.update(query, objArgs);
        } catch (Exception e) {
            log.error("Error al actualizar registros en tabla estado_sincr_bt: ", e);
            throw new RuntimeException();
        } finally {
            objArgs = null;
        }
    }
    
    private List<ArticuloVenta> getListArticulos(SqlRowSet srs) {
        List<ArticuloVenta> listaArticuloVenta = new ArrayList<ArticuloVenta>();

        if (srs != null) {
            while (srs.next()) {
                ArticuloVenta articulo = new ArticuloVenta();
                articulo.setCorrelativoVenta(srs.getInt("CORRELATIVO_VENTA"));
                articulo.setCodArticulo(srs.getString("COD_ARTICULO"));
                articulo.setDescRipley(srs.getString("DESC_RIPLEY"));
                articulo.setUnidades(srs.getString("UNIDADES"));
                articulo.setPrecio(srs.getString("PRECIO"));
                articulo.setCodDespacho(srs.getString("COD_DESPACHO"));
                articulo.setTipoDespacho(srs.getString("TIPO_DESPACHO"));
                articulo.setEstadoVenta(srs.getString("ESTADO_VENTA"));
                articulo.setFechaDespacho(srs.getString("FECHA_DESPACHO"));
                articulo.setImagen(srs.getString("TIPO_PAPEL_REGALO"));
                articulo.setEs_nc(srs.getString("ES_NC"));
                listaArticuloVenta.add(articulo);
            }
        }
        return listaArticuloVenta;
    }
    
    private ArticuloVenta getArticuloVenta(SqlRowSet srs){
        ArticuloVenta articulo = new ArticuloVenta();
        if (srs != null) {
            while (srs.next()) {
                articulo.setCorrelativoVenta(srs.getInt("CORRELATIVO_VENTA"));
                articulo.setCodArticulo(srs.getString("COD_ARTICULO"));
                articulo.setDescRipley(srs.getString("DESC_RIPLEY"));
                articulo.setUnidades(srs.getString("UNIDADES"));
                articulo.setPrecio(srs.getString("PRECIO"));
                articulo.setCodDespacho(srs.getString("COD_DESPACHO"));
                articulo.setTipoDespacho(srs.getString("TIPO_DESPACHO"));
                articulo.setEstadoVenta(srs.getString("ESTADO_VENTA"));
                articulo.setFechaDespacho(srs.getString("FECHA_DESPACHO"));
                articulo.setImagen(srs.getString("TIPO_PAPEL_REGALO"));
                articulo.setEs_nc(srs.getString("ES_NC"));
            }
        }
        return articulo;
    }

}
