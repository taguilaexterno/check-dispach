package com.ripley.commerce.dispatch.dao;

import java.util.List;

import com.ripley.commerce.dispatch.dto.ArticuloVenta;
import com.ripley.commerce.dispatch.dto.NotaVenta;
import com.ripley.commerce.dispatch.config.ApplicationParameters;

public interface CheckDispatchMEDao {

    public String findQuery(String idQuery);

    public List<NotaVenta> findAllOrderItems(String query, String wildCard, int jobID, int initRange, int endRange, int rowsLimit);
    
    public List<NotaVenta> actualizarArticulos(List<NotaVenta> lista);

    @Deprecated
    public List<NotaVenta> findAllOrderItems(ApplicationParameters parameters);

    public List<NotaVenta> findOrderItemsByOrderIds(ApplicationParameters parameters);

    public List<NotaVenta> findOrdersByOrdersId(ApplicationParameters parameters);

    public void editItem(String correlativoVenta, ArticuloVenta articuloVenta);

    public void updateSendEmailState(String correlativoVenta, int estadoEnvioCorreo, int tipoCorreo);

    public void updateCUDState(String correlativoVenta, int estado);
    public void updateCUDState1(String correlativoVenta, int estado);
    public void updateCUDState2(String correlativoVenta, int estado);
    
    public void updateNCState(int correlativoVenta, String codDespacho, int nuevoEstado, int estadoAModificar);
    
    public void actualizarRegistrosEnTablaEstadoSincronizacionBT(String query, int rows,  int initRange);

    public String findParameter(String parameterName, String applicationID);
}
