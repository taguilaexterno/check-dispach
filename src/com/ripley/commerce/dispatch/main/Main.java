package com.ripley.commerce.dispatch.main;

import com.ripley.commerce.dispatch.config.ApplicationParametersLoader;
import com.ripley.commerce.dispatch.config.ApplicationParameters;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ripley.commerce.dispatch.components.OrquestadorCheckDispatch;
import com.ripley.commerce.dispatch.config.ApplicationConfgurationLoader;
import com.ripley.commerce.dispatch.util.Constants;

public class Main {

    private static final Log LOGGER = LogFactory.getLog(Main.class);

    public static void main(String[] args) {
        try {
            LOGGER.info("Iniciando " + Constants.APP_NAME + " " + Constants.APP_VERSION);

            // INITIALIZE AND LOAD APP PARAMETERS
            ApplicationParameters.initialize(ApplicationParametersLoader.load(args));

            // INITIALIZE AND LOAD APP CONFIGURATIONS
            ApplicationConfgurationLoader.loadConfig();

            OrquestadorCheckDispatch orquestador = new OrquestadorCheckDispatch();
            orquestador.run();
        } catch (Exception ex) {
            LOGGER.error("Error fatal al intentar ejecutar la aplicación", ex);
        }
    }
}
