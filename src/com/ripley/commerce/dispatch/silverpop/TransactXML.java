package com.ripley.commerce.dispatch.silverpop;

import com.ripley.commerce.dispatch.dto.NotaVenta;

public interface TransactXML {
	public String sendEmail(String request);
	public String getXMLWithDataRetiro(NotaVenta notaVenta);
        public String getXMLWithDataNOK(NotaVenta notaVenta);
}
