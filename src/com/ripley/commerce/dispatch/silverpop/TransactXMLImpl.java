package com.ripley.commerce.dispatch.silverpop;


import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ripley.commerce.dispatch.dto.ArticuloVenta;
import com.ripley.commerce.dispatch.dto.NotaVenta;
import com.ripley.commerce.dispatch.util.Constants;
import com.ripley.commerce.dispatch.util.EncryptUtil;
import com.ripley.commerce.dispatch.util.Utils;

public class TransactXMLImpl implements TransactXML {

    private static final Log LOGGER = LogFactory.getLog(TransactXMLImpl.class);

    private String apiURLSilverpop;
    private String campaignIDNOK;
    private String campaignIDRetiro;
    //private int silverpopConnectionTimeout;
    //private int silverpopReadTimeout;
    private String silverpopConnectionTimeout;
    private String silverpopReadTimeout;

    @Override
    public String sendEmail(String request) {
        Map<String, String> requestProperty = new HashMap<>();
        requestProperty.put(Constants.CONTENT_TYPE_NAME, Constants.CONTENT_TYPE_VALUE_XML);

        String response = Utils.invokeURL(
                apiURLSilverpop,
                Constants.HTTP_METHOD_POST,
                request,
                requestProperty,
                getIntSilverpopConnectionTimeout(),
                getIntSilverpopReadTimeout());
        return response;
    }

    @Override
    public String getXMLWithDataRetiro(NotaVenta notaVenta) {
        return getXMLWithData(notaVenta, campaignIDRetiro);
    }

    @Override
    public String getXMLWithDataNOK(NotaVenta notaVenta) {
        return getXMLWithData(notaVenta, campaignIDNOK);
    }

    private String getXMLWithData(NotaVenta notaVenta, String campaignID) {

        String dispatchModalityRT = "";
        boolean tipoDespachoRT = false;
        boolean tipoDespachoST = false;
        
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(Constants.PREFIX_GDE);
        strBuilder.append(notaVenta.getCorrelativoVenta());
        strBuilder.append(Constants.SUFIJO_RT);
        
        String hashMd5 = EncryptUtil.getMD5(strBuilder.toString());
        String numberDisplay = EncryptUtil.hashToNumber(hashMd5, 6);
        

        String requestXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
                + "<XTMAILING>"
                + "<CAMPAIGN_ID>" + campaignID + "</CAMPAIGN_ID>"
                + "<TRANSACTION_ID>TRANS-" + notaVenta.getCorrelativoVenta() + "</TRANSACTION_ID>"
                + "<SHOW_ALL_SEND_DETAIL>true</SHOW_ALL_SEND_DETAIL>"
                + "<SEND_AS_BATCH>false</SEND_AS_BATCH>"
                + "<NO_RETRY_ON_FAILURE>false</NO_RETRY_ON_FAILURE>"
                + "<SAVE_COLUMNS>"
                + "<COLUMN_NAME>CAMPAIGN_ID</COLUMN_NAME>"
                + "<COLUMN_NAME>SUBJECT</COLUMN_NAME>"
                + "<COLUMN_NAME>ORDER_NUMBER</COLUMN_NAME>"
                + "<COLUMN_NAME>DISPATCH_MODALITY_RT</COLUMN_NAME>"
                + "</SAVE_COLUMNS>"
                + "<RECIPIENT>"
                //				+ "<EMAIL>"+emailTest+"</EMAIL>"
                + "<EMAIL>" + notaVenta.getEmailCliente() + "</EMAIL>"
                + "<BODY_TYPE>HTML</BODY_TYPE>"
                + "<PERSONALIZATION>"
                + "<TAG_NAME>CAMPAIGN_ID</TAG_NAME>"
                + "<VALUE>" + campaignID + "</VALUE>"
                + "</PERSONALIZATION>"
                + "<PERSONALIZATION>"
                + "<TAG_NAME>SUBJECT</TAG_NAME>"
                + "<VALUE>Ripley.com  Tus productos est n listos para Retirar</VALUE>"
                + "</PERSONALIZATION>"
                + "<PERSONALIZATION>"
                + "<TAG_NAME>STORE_NAME</TAG_NAME>"
                + "<VALUE>" + notaVenta.getNombreTienda() + "</VALUE>"
                + "</PERSONALIZATION>"
                + "<PERSONALIZATION>"
                + "<TAG_NAME>STORE_ID</TAG_NAME>"
                + "<VALUE>" + notaVenta.getCodBodega() + "</VALUE>"
                + "</PERSONALIZATION>"
                + "<PERSONALIZATION>"
                + "<TAG_NAME>DISPATCH_ADDRESS</TAG_NAME>"
                + "<VALUE>" + notaVenta.getDireccionDespacho() + "</VALUE>"
                + "</PERSONALIZATION>"
                + "<PERSONALIZATION>"
                + "<TAG_NAME>MAPA</TAG_NAME>"
                + "<VALUE><![CDATA[" + notaVenta.getUrlMapa() + "]]></VALUE>"
                + "</PERSONALIZATION>"
                + "<PERSONALIZATION>"
                + "<TAG_NAME>HORARIO</TAG_NAME>"
                + "<VALUE>" + notaVenta.getHorario() + "</VALUE>"
                + "</PERSONALIZATION>"
                + "<PERSONALIZATION>"
                + "<TAG_NAME>DATE_PLACED</TAG_NAME>"
                + "<VALUE>" + notaVenta.getFechaDespacho() + "</VALUE>"
                + "</PERSONALIZATION>"
                + "<PERSONALIZATION>"
                + "<TAG_NAME>ORDER_NUMBER</TAG_NAME>"
                + "<VALUE>" + notaVenta.getCorrelativoVenta() + "</VALUE>"
                + "</PERSONALIZATION>"
                + "<PERSONALIZATION>"
                + "<TAG_NAME>CLIENT_NAME</TAG_NAME>"
                + "<VALUE>" + notaVenta.getNombreDespacho() + "</VALUE>"
                + "</PERSONALIZATION>"
                + "<PERSONALIZATION>"
                + "<TAG_NAME>DISPATCH_MODALITY</TAG_NAME>"
                + "<VALUE>" + notaVenta.getTipoDespacho() + "</VALUE>"
                + "</PERSONALIZATION>"
                + "<PERSONALIZATION>"
                + "<TAG_NAME>VERIFICATION_CODE</TAG_NAME>"
                + "<VALUE>" + numberDisplay + "</VALUE>"
                + "</PERSONALIZATION>";

        for (int i = 0; i < notaVenta.getArticulos().size(); i++) {

            ArticuloVenta articuloVenta = notaVenta.getArticulos().get(i);

            if (Constants.JORNADA_DESPACHO_RT.equals(articuloVenta.getTipoDespacho())) {
                tipoDespachoRT = true;
                dispatchModalityRT = "<PERSONALIZATION>"
                        + "<TAG_NAME>DISPATCH_MODALITY_RT</TAG_NAME>"
                        + "<VALUE>" + Constants.JORNADA_DESPACHO_RT + "</VALUE>"
                        + "</PERSONALIZATION>";
            } else if (Constants.JORNADA_DESPACHO_ST.equals(articuloVenta.getTipoDespacho())) {
                tipoDespachoST = true;
                dispatchModalityRT = "<PERSONALIZATION>"
                        + "<TAG_NAME>DISPATCH_MODALITY_RT</TAG_NAME>"
                        + "<VALUE>" + Constants.JORNADA_DESPACHO_ST + "</VALUE>"
                        + "</PERSONALIZATION>";
            }

            if (tipoDespachoRT && tipoDespachoST) {
                dispatchModalityRT = "<PERSONALIZATION>"
                        + "<TAG_NAME>DISPATCH_MODALITY_RT</TAG_NAME>"
                        + "<VALUE>" + Constants.JORNADA_DESPACHO_MIX_RTST + "</VALUE>"
                        + "</PERSONALIZATION>";
            }

            requestXML = requestXML + "<PERSONALIZATION>"
                    + "<TAG_NAME>NAME_" + (i + 1) + "</TAG_NAME>"
                    + "<VALUE>" + articuloVenta.getDescRipley() + "</VALUE>"
                    + "</PERSONALIZATION>"
                    + "<PERSONALIZATION>"
                    + "<TAG_NAME>PARTNUMBER_" + (i + 1) + "</TAG_NAME>"
                    + "<VALUE>" + articuloVenta.getCodArticulo() + "</VALUE>"
                    + "</PERSONALIZATION>"
                    + "<PERSONALIZATION>"
                    + "<TAG_NAME>IMAGE URL_" + (i + 1) + "</TAG_NAME>"
                    + "<VALUE>" + articuloVenta.getImagen() + "</VALUE>"
                    + "</PERSONALIZATION>"
                    + "<PERSONALIZATION>"
                    + "<TAG_NAME>QUANTITY_" + (i + 1) + "</TAG_NAME>"
                    + "<VALUE>" + articuloVenta.getUnidades() + "</VALUE>"
                    + "</PERSONALIZATION>"
                    + "<PERSONALIZATION>"
                    + "<TAG_NAME>PRICE_" + (i + 1) + "</TAG_NAME>"
                    + "<VALUE>" + articuloVenta.getPrecio() + "</VALUE>"
                    + "</PERSONALIZATION>";
        }

        requestXML = requestXML + dispatchModalityRT;

        requestXML = requestXML + "</RECIPIENT>"
                + "</XTMAILING>";
        LOGGER.debug("CAMPAIGN_ID:" + campaignID + ";" + requestXML);
        return requestXML;
    }

    public void setApiURLSilverpop(String apiURLSilverpop) {
        this.apiURLSilverpop = apiURLSilverpop;
    }

    public void setCampaignIDNOK(String campaignIDNOK) {
        this.campaignIDNOK = campaignIDNOK;
    }

    public void setCampaignIDRetiro(String campaignIDRetiro) {
        this.campaignIDRetiro = campaignIDRetiro;
    }

    public void setSilverpopConnectionTimeout(String silverpopConnectionTimeout) {
        this.silverpopConnectionTimeout = silverpopConnectionTimeout;
    }

    public void setSilverpopReadTimeout(String silverpopReadTimeout) {
        this.silverpopReadTimeout = silverpopReadTimeout;
    }

    public int getIntSilverpopReadTimeout() {
        int value = 0;
        if (silverpopReadTimeout != null && !silverpopReadTimeout.isEmpty()) {
            if (NumberUtils.isNumber(silverpopReadTimeout)) {
                value = Integer.parseInt(silverpopReadTimeout);
            }
        }
        return value;
    }

    public int getIntSilverpopConnectionTimeout() {
        int value = 0;
        if (silverpopConnectionTimeout != null && !silverpopConnectionTimeout.isEmpty()) {
            if (NumberUtils.isNumber(silverpopConnectionTimeout)) {
                value = Integer.parseInt(silverpopConnectionTimeout);
            }
        }
        return value;
    }
    
}
