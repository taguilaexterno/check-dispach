/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ripley.commerce.dispatch.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ApplicationParametersLoader {

    private static final Log LOGGER = LogFactory.getLog(ApplicationParametersLoader.class);

    private static final int CANTIDAD_PARAMETROS_REQUERIDOS = 8;

    private static final String ERROR_PARAMETROS_INSUFICIENTES = "Parametros insuficientes o formato de parametros incorrecto. Aplicacion requiere de parametros:\n"
            + "<limite de registros : numero entero positivo>\n"
            + "<cantidad de dias atras desde fecha actual: {{+|-}numero entero}>\n"
            + "<cantidad de dias futuros desde fecha actual: {{+|-}numero entero}>\n"
            + "<activacion de proceso envio de correo 'Listo para retirar': {true|false}>\n"
            + "<activacion de proceso envio de correo 'Ups': {true|false}>\n"
            + "<activacion de proceso 'Actualizador de estados desde BT': {true|false}>\n"
            + "<identificador de Job Jenckins: entero numerico>\n"
            + "<identificador de consulta para 'Carga incial de OCs a procesar': Cadena de caracteres sin espacios>";

    public static ApplicationParameters load(String[] args) {
        try {
            ApplicationParametersLoader loader = new ApplicationParametersLoader();
            ApplicationParameters params = new ApplicationParameters();

            if (loader.cantidadParametrosEsperada(args)) {
                params.setRows(Integer.parseInt(args[0]));
                params.setInitDateDispatch(Integer.parseInt(args[1]));
                params.setEndDateDispatch(Integer.parseInt(args[2]));
                params.setEnableProcessEmailOK(Boolean.parseBoolean(args[3]));
                params.setEnableProcessEmailNOK(Boolean.parseBoolean(args[4]));
                params.setEnableProcessUpdater(Boolean.parseBoolean(args[5]));
//                params.setEnableProcessActualizarTablaESBT();
                params.setJobID(Integer.parseInt(args[6]));
                params.setInitializationQueryID(args[7].trim().toUpperCase());
                if (args != null && args.length == CANTIDAD_PARAMETROS_REQUERIDOS + 1) {
                    params.setWildcard(args[8].trim().toUpperCase());
                } else {
                    params.setWildcard(" ");
                }
            } else {
                throw new RuntimeException(ERROR_PARAMETROS_INSUFICIENTES);
            }

            return params;
        } catch (Exception ex) {
            throw ex;
        }
    }

    private boolean cantidadParametrosEsperada(String[] args) {
        boolean flag = false;
        if (args != null && args.length == CANTIDAD_PARAMETROS_REQUERIDOS) {
            flag = true;
        } else if (args != null && args.length == CANTIDAD_PARAMETROS_REQUERIDOS + 1) {
            flag = true;
        }
        return flag;
    }

//    private String obtenerSwitchs() {
//        LOG.debug("[METHOD] obtenerSwitchs");
//        String result = null;
//        try {
//            result = this.checkDispatchService.findQuery("CHECKDISPATCH_SWITCHS");
//        } catch (Exception e) {
//            LOG.error("Error obteniendo NAME CHECKDISPATCH_SWITCHS desde JEN_PARAMETERS en BD ME: ", e);
//        }
//        return result;
//    }
}
