/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ripley.commerce.dispatch.config;

import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

// TODO: implementar ApplicationConfgurationLoader agnostico del mecanismo de carga
public class ApplicationConfgurationLoader {

    private static final Log LOGGER = LogFactory.getLog(ApplicationConfgurationLoader.class);

    public static void loadConfig() throws ExceptionInInitializerError {
        try {
            ApplicationConfiguration appConfig = new ApplicationConfiguration();

            Map<String, String> map = getParametersFromDB();

            ApplicationConfiguration.setAppConfig(appConfig);
        } catch (Exception ex) {
            LOGGER.fatal("Error grave al intentar cargar la configuración de la aplicación", ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    private static Map<String, String> getParametersFromDB() {
        try {
            Map<String, String> map;
            return null;
        } catch (Exception ex) {
            throw ex;
        }
    }
}
