package com.ripley.commerce.dispatch.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Definicion de parametros
 */
public class ApplicationParameters {

    private static ApplicationParameters applicationParameters;
    @Deprecated
    private String initDate;
    // Limite de filas a retornar en consulta de obtencion de OCs
    private int rows;
    @Deprecated
    private String orderId;
    // Cantidad de dias anteriores (-) o posteriores (+) desde fecha de despacho
    private int initDateDispatch;
    // Cantidad de dias anteriores (-) o posteriores (+) desde fecha de despacho
    private int endDateDispatch;

    private String wildcard;

    // Indica si se debe ejecutar proceso de envio de correo "Listo para retirar"
    private boolean enableProcessEmailOK;
    // Indica si se debe ejecutar proceso de envio de correo "Ups" (Inconveniente en algun producto)
    private boolean enableProcessEmailNOK;
    // Indica si se debe ejecutar proceso de actualzacion de estados desde BigTicket
    private boolean enableProcessUpdater;
    // Indica si se debe ejecutar proceso de actualzacion de actualización de tabla estado_sincr_bt de BD ME.
    private boolean enableProcessActualizarTablaESBT;
    // ID auxiliar en instanciacion de tarea Jenkins
    private int jobID;
    // Identificador de consulta a utilizar, definida en tabla JEN_PARAMETERS
    private String initializationQueryID;

    public static void initialize(ApplicationParameters obj) {
        applicationParameters = obj;
    }

    public static ApplicationParameters get() {
        return applicationParameters;
    }

    public String getInitDate() {
        return initDate;
    }

    public void setInitDate(String initDate) {
        this.initDate = initDate;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getInitDateDispatch() {
        return initDateDispatch;
    }

    public void setInitDateDispatch(int initDateDispatch) {
        this.initDateDispatch = initDateDispatch;
    }

    public int getEndDateDispatch() {
        return endDateDispatch;
    }

    public void setEndDateDispatch(int endDateDispatch) {
        this.endDateDispatch = endDateDispatch;
    }

    public boolean isEnableProcessEmailOK() {
        return enableProcessEmailOK;
    }

    public void setEnableProcessEmailOK(boolean enableProcessEmailOK) {
        this.enableProcessEmailOK = enableProcessEmailOK;
    }

    public boolean isEnableProcessEmailNOK() {
        return enableProcessEmailNOK;
    }

    public void setEnableProcessEmailNOK(boolean enableProcessEmailNOK) {
        this.enableProcessEmailNOK = enableProcessEmailNOK;
    }

    public boolean isEnableProcessUpdater() {
        return enableProcessUpdater;
    }

    public void setEnableProcessUpdater(boolean enableProcessUpdater) {
        this.enableProcessUpdater = enableProcessUpdater;
    }
    
    public boolean isEnableProcessActualizarTablaESBT() {
        return enableProcessActualizarTablaESBT;
    }

    public void setEnableProcessActualizarTablaESBT(boolean enableProcessActualizarTablaESBT) {
        this.enableProcessActualizarTablaESBT = enableProcessActualizarTablaESBT;
    }

    public int getJobID() {
        return jobID;
    }

    public void setJobID(int jobID) {
        this.jobID = jobID;
    }

    public String getInitializationQueryID() {
        return initializationQueryID;
    }

    public void setInitializationQueryID(String initializationQueryID) {
        this.initializationQueryID = initializationQueryID;
    }

    public String getWildcard() {
        return wildcard;
    }

    public void setWildcard(String wildcard) {
        this.wildcard = wildcard;
    }

    @Override
    public String toString() {
        return "Parameters [initDate=" + initDate + ", rows=" + rows + ", orderId=" + orderId + ", initDateDispatch="
                + initDateDispatch + ", endDateDispatch=" + endDateDispatch + "]";
    }
}
