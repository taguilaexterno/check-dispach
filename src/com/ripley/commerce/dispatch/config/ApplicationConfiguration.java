/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ripley.commerce.dispatch.config;

/**
 *
 * @author dams
 */
public class ApplicationConfiguration {

    /*PARAMETER IDENTIFIERS FOR OBTAIN ITS VALUES*/
    public static final String BD_PARAMETER_APPLICATION_ID = "CHECKTRACKING_SIMPLE";
    public static final String BD_PARAMETER_SLA_EXPIRATION_TIME_RT = "SLA_EXPIRATION_TIME_RT";
    public static final String BD_PARAMETER_SLA_EXPIRATION_TIME_ST = "SLA_EXPIRATION_TIME_ST";

    /*SINGLE INSTANCE IN THE APP*/
    private static ApplicationConfiguration appConfig;

    /*ATRIBUTES*/
    private Integer expirationTimeSLART;
    private Integer expirationTimeSLAST;

    protected ApplicationConfiguration() {
    }

    protected static void setAppConfig(ApplicationConfiguration appConfigParam) {
        appConfig = appConfigParam;
    }

    public static ApplicationConfiguration get() {
        return appConfig;
    }

    /*SETTERS ONLY ACCESIBLE BY APPLICATION CONFIG LOADER CLASS*/
    public void setExpirationTimeSLART(Integer expirationTimeSLART) {
        this.expirationTimeSLART = expirationTimeSLART;
    }

    public void setExpirationTimeSLAST(Integer expirationTimeSLAST) {
        this.expirationTimeSLAST = expirationTimeSLAST;
    }

    /*GETTERS ACCESIBLE BY THE ENTIRE APPLICATION*/
    public Integer getExpirationTimeSLART() {
        return expirationTimeSLART;
    }

    public Integer getExpirationTimeSLAST() {
        return expirationTimeSLAST;
    }

}
