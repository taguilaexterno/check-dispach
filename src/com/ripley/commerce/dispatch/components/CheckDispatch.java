package com.ripley.commerce.dispatch.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.ripley.commerce.dispatch.dto.ArticuloVenta;
import com.ripley.commerce.dispatch.dto.Despacho;
import com.ripley.commerce.dispatch.dto.MailConfiguration;
import com.ripley.commerce.dispatch.dto.NotaVenta;
import com.ripley.commerce.dispatch.config.ApplicationParameters;
import com.ripley.commerce.dispatch.dto.TiendasRetiroEnTienda;
import com.ripley.commerce.dispatch.service.CheckDispatchService;
import com.ripley.commerce.dispatch.util.Constants;
import com.ripley.commerce.dispatch.util.MailUtils;
import com.ripley.commerce.dispatch.util.Utils;

@Deprecated
public class CheckDispatch {

    private static final Log LOG = LogFactory.getLog(CheckDispatch.class);

    private CheckDispatchService checkDispatchService;

    private List<NotaVenta> listNotaVentaInicial;
    private List<Despacho> listDespachos;
    private List<NotaVenta> listNotaVentaParaEnviarCorreo;
    private List<NotaVenta> listNotaVentaCompleta;

    private MailConfiguration mailConfiguration;

    private StringBuilder strBuilder;

    private Map<String, Integer> mapStatusDispatch;
    private Map<String, Integer> mapStatusEmails;
    private Map<String, String> mapErrors;

    private static final int MAX_NUMBER_QUERIES = 500;

    public CheckDispatch() {
        this.strBuilder = new StringBuilder();
        this.mapStatusDispatch = new HashMap<>();
        this.mapStatusEmails = new HashMap<>();
        this.mapErrors = new HashMap<>();
    }

    public void execute(ApplicationParameters parameters) {
        this.logInfoAndEmail("Iniciando CheckDispatch - " + Utils.getCurrentDate() + "\n");
        LOG.debug("parameters: " + parameters);

        //1- Busca los articulos con estado no final
        this.setlistNotaVentaInicial(parameters);

        if (listNotaVentaInicial != null & !listNotaVentaInicial.isEmpty()) {
            //2. Busca los CUDs en BigTicket
            this.setlistDespachos();

            if (listDespachos != null & !listDespachos.isEmpty()) {
                //3- Los CUDs encontrados se agregar a la lista de OC inicial
                this.mergeList();

                //4- Se actualizan los articulos con su estado venta actual (estado despacho de BT)
                this.updateItems();

                //5- Se completas los datos para enviar los correos (solo para ordenes completas Listo para Retirar ST/RT)
                this.completeDataForSendingEmail();

                //6- Se envian los emails por Transact XML
                this.sendEmails();

                //7- Se actualizan los articulos con el estado final del envio del correo (Correo Enviado/ Error Correo) 
                this.updateSentEmails();

                //8- Enviar email resumen a Soporte
                this.sendEmailSoporte();
            }
        }
    }

    private void setlistNotaVentaInicial(ApplicationParameters parameters) {
        LOG.debug("[METHOD] setlistNotaVentaInicial");
        if (parameters.getOrderId() != null && !"".equals(parameters.getOrderId().trim())) {
            LOG.debug("Buscar por ordersId: " + parameters.getOrderId());
            listNotaVentaInicial = this.checkDispatchService.findOrderItemsByOrderIds(parameters);
        } else {
            listNotaVentaInicial = this.checkDispatchService.findAllOrderItems(parameters);
        }

        if (listNotaVentaInicial != null & !listNotaVentaInicial.isEmpty()) {
            for (int i = 0; i < listNotaVentaInicial.size(); i++) {
                NotaVenta notaVenta = listNotaVentaInicial.get(i);
                checkCompletedOrderME(notaVenta);
            }
        }
    }

    private void checkCompletedOrderME(NotaVenta notaVenta) {

        notaVenta.setOrdenCompleta(true);

        for (int i = 0; i < notaVenta.getArticulos().size(); i++) {
            ArticuloVenta articuloVenta = notaVenta.getArticulos().get(i);

            //Si la orden tiene articulos con estado venta distinto a los siguientes
            //Entonces es una orden con estados donde no se enviara el correo
            if (!Constants.DISPATCH_STATE_RT_STOCK_CONFIRMADO.equals(articuloVenta.getEstadoVenta())
                    && !Constants.DISPATCH_STATE_ST_PREPARANDO_PRODUCTO.equals(articuloVenta.getEstadoVenta())
                    && !Constants.DISPATCH_STATE_ST_RECIBIDO_EN_TIENDA.equals(articuloVenta.getEstadoVenta())
                    && !Constants.DISPATCH_STATE_ST_LISTO_PARA_RETIRAR.equals(articuloVenta.getEstadoVenta())
                    && !Constants.DISPATCH_STATE_ST_EN_TRANSITO_A_TIENDA.equals(articuloVenta.getEstadoVenta())
                    && !Constants.DISPATCH_STATE_ST_PRODUCTO_RETIRADO.equals(articuloVenta.getEstadoVenta())
                    && !Constants.DISPATCH_STATE_ERROR_CORREO.equals(articuloVenta.getEstadoVenta())
                    && !Constants.DISPATCH_STATE_CUD_NO_EXISTE.equals(articuloVenta.getEstadoVenta())
                    //Si es ST y el estado venta es numerico, esta OK
                    && !(Constants.JORNADA_DESPACHO_ST.equals(articuloVenta.getTipoDespacho())
                    && Utils.isNumeric(articuloVenta.getEstadoVenta()))) {
                notaVenta.setOrdenCompleta(false);
                break;
            }
        }
    }

    private void setlistDespachos() {
        LOG.debug("[METHOD] setlistCUDsBT");
        this.logInfoAndEmail("\n\nTotal OCs pendientes: " + listNotaVentaInicial.size());

        String cuds = "";
        listDespachos = new ArrayList<Despacho>();
        int count = 0;
        for (int i = 0; i < listNotaVentaInicial.size(); i++) {
            NotaVenta notaVenta = listNotaVentaInicial.get(i);

            //Si la orden se encuentra sin estados para no enviar correo
            //Entonces buscar los estados de los CUDs en BigTicket
            if (notaVenta.isOrdenCompleta()) {

                for (int j = 0; j < notaVenta.getArticulos().size(); j++) {
                    ArticuloVenta articulo = notaVenta.getArticulos().get(j);
                    if (count == 0) {
                        cuds = articulo.getCodDespacho();
                        count++;
                    } else {
                        cuds = cuds + "," + articulo.getCodDespacho();
                        count++;
                    }

                    if (count == MAX_NUMBER_QUERIES) {
                        listDespachos.addAll(this.checkDispatchService.findDispatchStatusByCUDs(cuds));
                        LOG.debug("Total listDespachos: " + listDespachos.size());
                        count = 0;
                        cuds = "";
                    }
                }
            }
        }
//		log.debug("cuds: " + cuds);

        //Revisamos si son menos de 500 o el sobrante que no llego a 500
        if (!"".equals(cuds)) {
            listDespachos.addAll(this.checkDispatchService.findDispatchStatusByCUDs(cuds));
        }

        if (!listDespachos.isEmpty()) {
            this.logInfoAndEmail("\nTotal CUDs en BT: " + listDespachos.size());
        } else {
            this.logInfoAndEmail("\nTotal CUDs en BT: 0");
        }
//		log.debug("listCUDs: " + listDespachos);
    }

    public void mergeList() {
        LOG.debug("[METHOD] mergeList");

        for (int i = 0; i < listNotaVentaInicial.size(); i++) {
            NotaVenta notaVenta = listNotaVentaInicial.get(i);

            if (notaVenta.isOrdenCompleta()) {

                for (int j = 0; j < notaVenta.getArticulos().size(); j++) {
                    ArticuloVenta articuloVenta = notaVenta.getArticulos().get(j);
                    for (int k = 0; k < listDespachos.size(); k++) {
                        Despacho despacho = listDespachos.get(k);
                        if (despacho.getCud().equals(articuloVenta.getCodDespacho())) {
                            articuloVenta.setExisteEnBT(true);
                            this.interpreterState(articuloVenta, despacho);
                            break;
                        }
                    }
                }
                this.checkCompletedOrderBT(notaVenta);

            }
        }

//		log.debug("listNotaVentaInicial: " + listNotaVentaInicial);
        if (listNotaVentaParaEnviarCorreo != null) {
            this.logInfoAndEmail("\nTotal correos Listos para Retirar: " + listNotaVentaParaEnviarCorreo.size());
            LOG.debug("listNotaVentaParaEnviarCorreo: " + listNotaVentaParaEnviarCorreo);
            TiendasRetiroEnTienda tiendas = new TiendasRetiroEnTienda();

            for (int i = 0; i < listNotaVentaParaEnviarCorreo.size(); i++) {
                NotaVenta notaVenta = listNotaVentaParaEnviarCorreo.get(i);
                try {
                    notaVenta.setUrlMapa(tiendas.get(Integer.parseInt(notaVenta.getCodBodega())).getUrlMapa());
                    notaVenta.setHorario(tiendas.get(Integer.parseInt(notaVenta.getCodBodega())).getHorario());
                    //Ejemplo: Costanera Center Avenida Andres Bello 2422 REG.METROPOLITANA LAS CONDES
                    notaVenta.setDireccionDespacho(tiendas.get(Integer.parseInt(notaVenta.getCodBodega())).getDireccion());
                } catch (Exception ex) {
                    LOG.warn("Error datos tienda RT: " + ex.getMessage() + " - notaVenta" + notaVenta);
                }

                for (int j = 0; j < notaVenta.getArticulos().size(); j++) {
                    ArticuloVenta articuloVenta = notaVenta.getArticulos().get(j);
                    //CAMBIOS PARA OBTENER LA IMAGEN DE ARTICULO_VENTA, TIPO_PAPEL_REGALO
                    if (articuloVenta.getImagen() == null || "".equals(articuloVenta.getImagen())) {
                        //Si la imagen del producto no se encuentra en BD, buscarla a traves del rest productview
                        articuloVenta.setImagen(this.transformItemImageCorrect(checkDispatchService.findThumbnailImageItem(articuloVenta.getCodArticulo())));
                    } else {
                        articuloVenta.setImagen(this.transformItemImageCorrect(articuloVenta.getImagen()));
                    }
                }
            }
        }
    }

    private void interpreterState(ArticuloVenta articuloVenta, Despacho despacho) {

        //Si la jornada en BT es distinto a ST o RT, se debe a una solucion comercial
        if (!Constants.JORNADA_DESPACHO_ST.equals(despacho.getJornadaDesp())
                && !Constants.JORNADA_DESPACHO_RT.equals(despacho.getJornadaDesp())) {
            articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_JORNADA_DISTINTA);
//			this.mapReduceString(mapErrors,articuloVenta.getNuevoEstadoVenta(), articuloVenta.getCodDespacho());
        } else if (Constants.JORNADA_DESPACHO_ST.equals(despacho.getJornadaDesp())
                && despacho.getCodMotivo() == null && "3".equals(despacho.getEstadoDespacho())) {
            articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_ST);
            articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_ST_PRODUCTO_RETIRADO);
        } else if (Constants.JORNADA_DESPACHO_RT.equals(despacho.getJornadaDesp())
                && despacho.getCodMotivo() == null && "3".equals(despacho.getEstadoDespacho())) {
            articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_RT);
            articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_RT_PRODUCTO_RETIRADO);
        } else if (Constants.JORNADA_DESPACHO_ST.equals(despacho.getJornadaDesp())
                && "30".equals(despacho.getCodMotivo()) && "0".equals(despacho.getEstadoDespacho())) {
            articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_ST);
            articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_ST_LISTO_PARA_RETIRAR);
        } else if (Constants.JORNADA_DESPACHO_ST.equals(despacho.getJornadaDesp())
                && "21".equals(despacho.getCodMotivo()) && "0".equals(despacho.getEstadoDespacho())) {
            articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_ST);
            articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_ST_RECIBIDO_EN_TIENDA);
        } else if (Constants.JORNADA_DESPACHO_ST.equals(despacho.getJornadaDesp())
                && despacho.getCodMotivo() == null && "C".equals(despacho.getEstadoDespacho())) {
            articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_ST);
            articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_ST_EN_TRANSITO_A_TIENDA);
        } else if (Constants.JORNADA_DESPACHO_ST.equals(despacho.getJornadaDesp())
                && despacho.getCodMotivo() == null && "1".equals(despacho.getEstadoDespacho())) {
            articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_ST);
            articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_ST_PREPARANDO_PRODUCTO);
        } else if (Constants.JORNADA_DESPACHO_ST.equals(despacho.getJornadaDesp())
                && despacho.getCodMotivo() == null && "0".equals(despacho.getEstadoDespacho())) {
            articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_ST);
            articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_ST_CREACION_ORDEN_COMPRA);
        } else if (Constants.JORNADA_DESPACHO_RT.equals(despacho.getJornadaDesp())
                && despacho.getCodMotivo() == null && "0".equals(despacho.getEstadoDespacho())) {
            articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_RT);
            articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_RT_LISTO_PARA_RETIRAR);
        } else {
            articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_SIN_CAMBIOS);
        }
        this.mapReduceInt(mapStatusDispatch, articuloVenta.getNuevoEstadoVenta());
//		log.debug("interpreter: " + articuloVenta.getNuevoEstadoVenta());
    }

    private void checkCompletedOrderBT(NotaVenta notaVenta) {
//		log.debug("[METHOD] checkCompletedOrder");
        boolean checkCompletedOrder = true;
        boolean productoRetirado = false;
        boolean itemST = false;

        //Revisando ST
        for (int i = 0; i < notaVenta.getArticulos().size(); i++) {
            ArticuloVenta articuloVenta = notaVenta.getArticulos().get(i);
            if ("ST".equals(articuloVenta.getTipoDespacho())) {
                itemST = true;
                //Si no se encuentra el CUD en BT
                if (!articuloVenta.isExisteEnBT()) {
                    checkCompletedOrder = false;
                    articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_CUD_NO_EXISTE);
                    this.mapReduceString(mapErrors, articuloVenta.getNuevoEstadoVenta(), articuloVenta.getCodDespacho());
                    break;
                }
                //Si el producto ST no se encuentra Listo para Retirar, no enviar correo
                if (!Constants.DISPATCH_STATE_ST_LISTO_PARA_RETIRAR.equals(articuloVenta.getNuevoEstadoVenta())) {
                    checkCompletedOrder = false;
                    break;
                }
            }
        }

        //Revisando RT
        if (checkCompletedOrder) {
            for (int i = 0; i < notaVenta.getArticulos().size(); i++) {
                ArticuloVenta articuloVenta = notaVenta.getArticulos().get(i);
                if ("RT".equals(articuloVenta.getTipoDespacho())) {
                    //Si no se encuentra el CUD en BT
                    if (!articuloVenta.isExisteEnBT()) {
                        articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_CUD_NO_EXISTE);
                        this.mapReduceString(mapErrors, articuloVenta.getNuevoEstadoVenta(), articuloVenta.getCodDespacho());
                        checkCompletedOrder = false;
                        break;
                    }
                    //Si existe ST y producto RT se encuentra Retirado, enviar correo
                    if (itemST && Constants.DISPATCH_STATE_RT_PRODUCTO_RETIRADO.equals(articuloVenta.getNuevoEstadoVenta())) {
                        continue;
                    }
                    //Si no existe ST y producto RT se encuentra Retirado, no enviar correo 
                    if (!itemST && Constants.DISPATCH_STATE_RT_PRODUCTO_RETIRADO.equals(articuloVenta.getNuevoEstadoVenta())) {
                        productoRetirado = true;
                        continue;
                    }
                    //Si producto RT se encuentra Listo para Retirar, enviar correo
                    if (!Constants.DISPATCH_STATE_RT_LISTO_PARA_RETIRAR.equals(articuloVenta.getNuevoEstadoVenta())) {
                        checkCompletedOrder = false;
                        break;
                    }
                }
            }
        }

        if (checkCompletedOrder && !productoRetirado) {

            if (listNotaVentaParaEnviarCorreo == null) {
                listNotaVentaParaEnviarCorreo = new ArrayList<NotaVenta>();
            }
            notaVenta.setOrdenCompleta(true);
            listNotaVentaParaEnviarCorreo.add(notaVenta);
        }

    }

    private String transformItemImageCorrect(String urlImage) {
//		log.debug("[METHOD] transformItemImageCorrect");
        String url = "";
        if (urlImage != null && !"".equals(urlImage)) {
            if (urlImage.indexOf("ripleycl_CAT_AS/") != -1) {
                url = "http://home.ripley.cl/store/Attachment/".concat(urlImage.split("ripleycl_CAT_AS/")[1]);
            } else {
                url = "http://home.ripley.cl/store/Attachment/".concat(urlImage);
            }

        }
        return url;
    }

    private void updateItems() {
        LOG.debug("[METHOD] updateItems");
        if (listNotaVentaInicial != null) {
            for (int i = 0; i < listNotaVentaInicial.size(); i++) {
                NotaVenta notaVenta = listNotaVentaInicial.get(i);
                for (int j = 0; j < notaVenta.getArticulos().size(); j++) {
                    ArticuloVenta articuloVenta = notaVenta.getArticulos().get(j);

                    //Si el estado nuevo es distinto de null
                    //Si el estado inicial es distinto a Creacion Orden de Compra
                    //Si el estado inicial es distinto a Preparando Producto
                    //Si el estado inicial es distinto a Sin cambios de estado
                    if (articuloVenta.getNuevoEstadoVenta() != null
                            && !Constants.DISPATCH_STATE_ST_CREACION_ORDEN_COMPRA.equals(articuloVenta.getNuevoEstadoVenta())
                            && !Constants.DISPATCH_STATE_ST_PREPARANDO_PRODUCTO.equals(articuloVenta.getNuevoEstadoVenta())
                            && !Constants.DISPATCH_STATE_SIN_CAMBIOS.equals(articuloVenta.getNuevoEstadoVenta())
                            && !articuloVenta.getEstadoVenta().equals(articuloVenta.getNuevoEstadoVenta())) {
                        checkDispatchService.editItem(notaVenta.getCorrelativoVenta(), articuloVenta);
                    }
                }
            }
        }
    }

    private void completeDataForSendingEmail() {
        LOG.debug("[METHOD] completeDataForSendingEmail");

        if (listNotaVentaParaEnviarCorreo != null) {

            listNotaVentaCompleta = new ArrayList<NotaVenta>();
            ApplicationParameters parameters = null;
            String ordersId = "";
            int count = 0;

            for (int i = 0; i < listNotaVentaParaEnviarCorreo.size(); i++) {
                NotaVenta notaVenta = listNotaVentaParaEnviarCorreo.get(i);
                if (count == 0) {
                    ordersId = notaVenta.getCorrelativoVenta();
                    count++;
                } else {
                    ordersId = ordersId + "," + notaVenta.getCorrelativoVenta();
                    count++;
                }

                if (count == MAX_NUMBER_QUERIES) {
                    parameters = new ApplicationParameters();
                    parameters.setOrderId(ordersId);
                    listNotaVentaCompleta.addAll(this.checkDispatchService.findOrdersByOrdersId(parameters));
                    LOG.debug("Total NotaVenta: " + listNotaVentaCompleta.size());
                    count = 0;
                    ordersId = "";
                }
            }

            //Revisamos si son menos de 500 o el sobrante que no llego a 500
            if (!"".equals(ordersId)) {
                parameters = new ApplicationParameters();
                parameters.setOrderId(ordersId);
                listNotaVentaCompleta.addAll(this.checkDispatchService.findOrdersByOrdersId(parameters));
                LOG.debug("Total NotaVenta: " + listNotaVentaCompleta.size());
            }

            for (int i = 0; i < listNotaVentaParaEnviarCorreo.size(); i++) {
                NotaVenta notaVentaVacia = listNotaVentaParaEnviarCorreo.get(i);

                for (int j = 0; j < listNotaVentaCompleta.size(); j++) {
                    NotaVenta notaVenta = listNotaVentaCompleta.get(j);
                    if (notaVentaVacia.getCorrelativoVenta().equals(notaVenta.getCorrelativoVenta())) {
                        notaVentaVacia.setEmailCliente(notaVenta.getEmailCliente());
                        notaVentaVacia.setNombreTienda(notaVenta.getNombreTienda());
                        notaVentaVacia.setDireccionDespacho(notaVentaVacia.getDireccionDespacho() + ", " + notaVenta.getRegionDespacho());
                        notaVentaVacia.setFechaDespacho(notaVenta.getFechaDespacho());
                        notaVentaVacia.setNombreDespacho(notaVenta.getNombreDespacho());
                        notaVentaVacia.setTipoDespacho(notaVenta.getTipoDespacho());
                        notaVentaVacia.setComunaDespacho(notaVenta.getComunaDespacho());
                        notaVentaVacia.setRegionDespacho(notaVenta.getRegionDespacho());
                    }
                }
            }
        }
//		log.debug("listNotaVentaParaEnviarCorreo: " + listNotaVentaParaEnviarCorreo);
    }

    private void sendEmails() {
        LOG.debug("[METHOD] sendEmails");

        if (listNotaVentaParaEnviarCorreo != null) {
            for (int i = 0; i < listNotaVentaParaEnviarCorreo.size(); i++) {
                NotaVenta notaVenta = listNotaVentaParaEnviarCorreo.get(i);
                //String requestXML = this.checkDispatchService.getXMLWithData(notaVenta);
                String requestXML = "";
                String response = this.checkDispatchService.sendEmail(requestXML);
//				log.debug("response: " + response);
                String status = response.split("<STATUS>")[1].split("</STATUS>")[0];
                if ("0".equals(status)) {
                    for (int j = 0; j < notaVenta.getArticulos().size(); j++) {
                        ArticuloVenta articuloVenta = notaVenta.getArticulos().get(j);
                        articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_CORREO_ENVIADO);
                        articuloVenta.setCorreoEnviado(true);
                        this.mapReduceInt(mapStatusEmails, articuloVenta.getNuevoEstadoVenta());
                    }
                } else {
                    for (int j = 0; j < notaVenta.getArticulos().size(); j++) {
                        ArticuloVenta articuloVenta = notaVenta.getArticulos().get(j);
                        articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_ERROR_CORREO);
                        articuloVenta.setCorreoEnviado(false);
                        this.mapReduceInt(mapStatusEmails, articuloVenta.getNuevoEstadoVenta());
                        this.mapReduceString(mapErrors, articuloVenta.getNuevoEstadoVenta(), articuloVenta.getCodDespacho());
                    }
                    LOG.debug("ERROR ENVIO MAIL: " + notaVenta + " - requestXML: "
                            + requestXML + " - response: " + response);
                }
            }
        }
//		log.debug("listNotaVentaParaEnviarCorreo: " + listNotaVentaParaEnviarCorreo);
    }

    private void updateSentEmails() {
        LOG.debug("[METHOD] updateSentEmails");
        if (listNotaVentaParaEnviarCorreo != null) {
            for (int i = 0; i < listNotaVentaParaEnviarCorreo.size(); i++) {
                NotaVenta notaVenta = listNotaVentaParaEnviarCorreo.get(i);
                for (int j = 0; j < notaVenta.getArticulos().size(); j++) {
                    ArticuloVenta articuloVenta = notaVenta.getArticulos().get(j);
                    //Actualizar con el estado del envio del correo (Correo Enviado/Error Correo)
                    checkDispatchService.editItem(notaVenta.getCorrelativoVenta(), articuloVenta);
                }
            }

        }
    }

    private void sendEmailSoporte() {
        LOG.debug("[METHOD] enviarEmailSoporte");

        this.logInfoAndEmail("\n\n----------------------------------");
        this.logInfoAndEmail("\nRESUMEN ESTADOS CUDS");
        this.logInfoAndEmail("\n----------------------------------");
        this.prepareCounterInformation(mapStatusDispatch);
        this.logInfoAndEmail("\n----------------------------------");

        this.logInfoAndEmail("\n\n----------------------------------");
        this.logInfoAndEmail("\nRESUMEN ESTADOS EMAILS");
        this.logInfoAndEmail("\n----------------------------------");
        this.prepareCounterInformation(mapStatusEmails);
        this.logInfoAndEmail("\n----------------------------------");

        this.logInfoAndEmail("\n\n----------------------------------");
        this.logInfoAndEmail("\nCUDS CON ERROR");
        this.logInfoAndEmail("\n----------------------------------");
        this.prepareStringInformation(mapErrors);
        this.logInfoAndEmail("\n----------------------------------");

        this.logInfoAndEmail("\n\nFIN - " + Utils.getCurrentDate());

        this.mailConfiguration.setSubject(Constants.SUBJECT_EMAIL);
        this.mailConfiguration.setMensaje(strBuilder.toString());
        MailUtils.envia(this.mailConfiguration);

    }

    private void mapReduceInt(Map<String, Integer> map, String status) {
        if (map.containsKey(status)) {
            map.put(status, map.get(status) + 1);
            return;
        }
        map.put(status, 1);
    }

    private void mapReduceString(Map<String, String> map, String status, String cud) {
        if (map.containsKey(status)) {
            map.put(status, map.get(status) + ", " + cud);
            return;
        }
        map.put(status, cud);
    }

    private void prepareCounterInformation(Map<String, Integer> map) {
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>) it.next();
            this.logInfoAndEmail("\n" + entry.getKey() + ": " + entry.getValue());
        }
    }

    private void prepareStringInformation(Map<String, String> map) {
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
            this.logInfoAndEmail("\n" + entry.getKey() + ": " + entry.getValue());
        }
    }

    private void logInfoAndEmail(String message) {
        LOG.info(message);
        strBuilder.append(message);
    }

    public void setCheckDispatchService(CheckDispatchService checkDispatchService) {
        this.checkDispatchService = checkDispatchService;
    }

    public void setMailConfiguration(MailConfiguration mailConfiguration) {
        this.mailConfiguration = mailConfiguration;
    }

}
