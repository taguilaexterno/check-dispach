/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ripley.commerce.dispatch.components.business.filters;

import com.ripley.commerce.dispatch.dto.ArticuloVenta;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dams
 */
public class FiltradorArticuloVenta {

    public static final int FILTRO_ESTADO_FINAL = 1;
    public static final int FILTRO_ESTADO_FINAL_OK = 2;
    public static final int FILTRO_ESTADO_FINAL_NOK = 3;
    public static final int FILTRO_ESTADO_FINAL_DESPACHO_RT = 4;

    public static final String JORNADA_RT = "RT";
    public static final String JORNADA_ST = "ST";

    /*Estados terminales pre-entrega OK*/
    private static final String ESTADO_LISTO_PARA_RETIRAR = "LISTO PARA RETIRAR";

    /*Estados terminales pre-entrega NOK*/
    private static final String ESTADO_SIN_STOCK = "SIN STOCK";
    private static final String ESTADO_RESERVA_CADUCADA = "RESERVA CADUCADA";
    private static final String ESTADO_RESERVA_CANCELADA = "RESERVA CANCELADA";
    private static final String ESTADO_CUD_NO_EXISTE = "CUD NO EXISTE EN BT";

    public static List<ArticuloVenta> filtrarLista(int idCriterio, List<ArticuloVenta> listaEntrada) {
        List<ArticuloVenta> listaSalida = null;

        if (listaEntrada != null && !listaEntrada.isEmpty()) {
            switch (idCriterio) {
                case FILTRO_ESTADO_FINAL:
                    listaSalida = filtrarListaPorEstadoFinal(listaEntrada);
                    break;
                case FILTRO_ESTADO_FINAL_OK:
                    listaSalida = filtrarListaPorEstadoTerminalOK(listaEntrada);
                    break;
                case FILTRO_ESTADO_FINAL_NOK:
                    listaSalida = filtrarListaPorEstadoFinalNOK(listaEntrada);
                    break;
                case FILTRO_ESTADO_FINAL_DESPACHO_RT:
                    listaSalida = filtrarListaPorEstadoFinalConDespachoRT(listaEntrada);
                    break;
                default:
                    // Se retorna la lista de entrada sin aplicar filtros
                    listaSalida = listaEntrada;
            }
        }

        return listaSalida;
    }

    private static List<ArticuloVenta> filtrarListaPorEstadoTerminalOK(List<ArticuloVenta> listaEntrada) {
        List<ArticuloVenta> listaSalida = new ArrayList<>();
        for (ArticuloVenta item : listaEntrada) {
            if (esArticuloConEstadoTerminalOK(item)) {
                listaSalida.add(item);
            }
        }
        if (listaSalida.isEmpty()) {
            listaSalida = null;
        }
        return listaSalida;
    }

    private static List<ArticuloVenta> filtrarListaPorEstadoFinalNOK(List<ArticuloVenta> listaEntrada) {
        List<ArticuloVenta> listaSalida = new ArrayList<>();
        for (ArticuloVenta item : listaEntrada) {
            if (esArticuloConEstadoTerminalNOK(item)) {
                listaSalida.add(item);
            }
        }
        if (listaSalida.isEmpty()) {
            listaSalida = null;
        }
        return listaSalida;
    }

    private static List<ArticuloVenta> filtrarListaPorEstadoFinal(List<ArticuloVenta> listaEntrada) {
        List<ArticuloVenta> listaSalida = new ArrayList<>();
        for (ArticuloVenta item : listaEntrada) {
            if (esArticuloConEstadoTerminal(item)) {
                listaSalida.add(item);
            }
        }
        if (listaSalida.isEmpty()) {
            listaSalida = null;
        }
        return listaSalida;
    }

    private static List<ArticuloVenta> filtrarListaPorEstadoFinalConDespachoRT(List<ArticuloVenta> listaEntrada) {
        List<ArticuloVenta> listaSalida = new ArrayList<>();
        for (ArticuloVenta item : listaEntrada) {
            if (esArticuloRTConEstadoTerminal(item)) {
                listaSalida.add(item);
            }
        }
        return listaSalida;
    }

    public static boolean esArticuloConEstadoTerminal(ArticuloVenta articuloVenta) {
        boolean flag = false;
        if (esArticuloConEstadoTerminalOK(articuloVenta)
                || esArticuloConEstadoTerminalNOK(articuloVenta)) {
            flag = true;
        }
        return flag;
    }

    public static boolean esArticuloRTConEstadoTerminal(ArticuloVenta articuloVenta) {
        boolean flag = false;
        if (esArticuloConEstadoTerminal(articuloVenta) && esArticuloConDespachoRT(articuloVenta)) {
            flag = true;
        }
        return flag;
    }

    private static boolean esArticuloConEstadoTerminalOK(ArticuloVenta articuloVenta) {
        boolean flag = false;
        if (articuloVenta.getEstadoVenta() != null) {
            if (articuloVenta.getEstadoVenta().trim().toUpperCase().equals(ESTADO_LISTO_PARA_RETIRAR)) {
                flag = true;
            }
        }
        return flag;
    }

    private static boolean esArticuloConEstadoTerminalNOK(ArticuloVenta articuloVenta) {
        boolean flag = false;
        String tmpEstado = null;
        String tmpJornada = null;
        if (articuloVenta.getEstadoVenta() != null) {
            tmpEstado = articuloVenta.getEstadoVenta().trim().toUpperCase();
            if (tmpEstado.equals(ESTADO_SIN_STOCK)
                    || tmpEstado.equals(ESTADO_RESERVA_CADUCADA)
                    || tmpEstado.equals(ESTADO_RESERVA_CANCELADA)
                    || tmpEstado.equals(ESTADO_CUD_NO_EXISTE)) {
                //tmpJornada = item.get
                if (tmpJornada != null) {
                    if (tmpJornada.equals(JORNADA_RT)) {
                        //validacion 48 hrs
                        flag = true;
                    } else if ((tmpJornada.equals(JORNADA_ST))) {
                        //validacion 21 hrs
                        flag = true;
                    }
                } else {
                    // no considerar cuando se implemente logica de SLA
                    flag = true;
                }
            }
        }
        return flag;
    }

    private static boolean esArticuloConDespachoRT(ArticuloVenta articuloVenta) {
        boolean flag = false;
        if (JORNADA_RT.equals(articuloVenta.getTipoDespacho())) {
            flag = true;
        }
        return flag;
    }
}
