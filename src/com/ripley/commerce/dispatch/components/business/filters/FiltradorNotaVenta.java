/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ripley.commerce.dispatch.components.business.filters;

import com.ripley.commerce.dispatch.dto.ArticuloVenta;
import com.ripley.commerce.dispatch.dto.NotaVenta;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dams
 */
public class FiltradorNotaVenta {

    public static final int FILTRO_ESTADO_FINAL = 1;
    public static final int FILTRO_DESPACHO_FULL_RT = 2;
    public static final int FILTRO_DESPACHO_FULL_ST = 3;
    public static final int FILTRO_DESPACHO_RT_ST = 4;
    public static final int FILTRO_ESTADO_NO_TERMINAL = 5;

    public static List<NotaVenta> filtrarLista(int idCriterio, List<NotaVenta> listaEntrada) {
        List<NotaVenta> listaSalida = null;

        if (listaEntrada != null && !listaEntrada.isEmpty()) {
            switch (idCriterio) {
                case FILTRO_ESTADO_FINAL:
                    listaSalida = filtrarListaPorEstadoFinal(listaEntrada);
                    break;
                case FILTRO_ESTADO_NO_TERMINAL:
                    listaSalida = filtrarListaPorEstadoNoTerminal(listaEntrada);
                    break;
                default:
                    // Se retorna la lista de entrada sin aplicar filtros
                    listaSalida = listaEntrada;
            }
        }

        return listaSalida;
    }

    private static List<NotaVenta> filtrarListaPorEstadoFinal(List<NotaVenta> listaEntrada) {
        List<NotaVenta> listaSalida = new ArrayList<>();
        boolean flag = true;
        for (NotaVenta notaVenta : listaEntrada) {
            List<ArticuloVenta> articulos = notaVenta.getArticulos();

            for (ArticuloVenta articulo : articulos) {
                if (!FiltradorArticuloVenta.esArticuloConEstadoTerminal(articulo)) {
                    flag = false;
                    break;
                }
            }
            if (flag) {
                listaSalida.add(notaVenta);
            }
            flag = true;

        }
//        if (listaSalida.isEmpty()) {
//            listaSalida = null;
//        }
        return listaSalida;
    }

    private static List<NotaVenta> filtrarListaPorEstadoNoTerminal(List<NotaVenta> listaEntrada) {
        List<NotaVenta> listaSalida = new ArrayList<>();
        boolean flag = true;
        for (NotaVenta notaVenta : listaEntrada) {
            List<ArticuloVenta> articulos = notaVenta.getArticulos();

            for (ArticuloVenta articulo : articulos) {
                if (!FiltradorArticuloVenta.esArticuloConEstadoTerminal(articulo)) {
                    flag = false;
                    break;
                }
            }
            if (!flag) {
                listaSalida.add(notaVenta);
            }
            flag = true;

        }
//        if (listaSalida.isEmpty()) {
//            listaSalida = null;
//        }
        return listaSalida;
    }

    public static boolean esNotaVentaEnEstadoTerminal(NotaVenta notaVenta) {
        boolean flag = true;

        List<ArticuloVenta> articulos = notaVenta.getArticulos();

        for (ArticuloVenta articulo : articulos) {
            if (!FiltradorArticuloVenta.esArticuloConEstadoTerminal(articulo)) {
                flag = false;
                break;
            }
        }

        return flag;
    }
}
