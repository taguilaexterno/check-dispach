/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ripley.commerce.dispatch.components;

import com.ripley.commerce.dispatch.dto.NotaVenta;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author dams
 */
public class RepositorioVolatilNotaVenta {

    public static boolean procesarEnvioCorreo = false;

    private static volatile List<NotaVenta> list = new ArrayList<>();

    public static synchronized void put(List<NotaVenta> listaNotaVenta) throws InterruptedException {
        list.addAll(listaNotaVenta);
    }

    public static synchronized void put(NotaVenta notaVenta) throws InterruptedException {
        list.add(notaVenta);
    }

    public static synchronized List<NotaVenta> getFullList() throws InterruptedException {
        return list;
    }

    /*
    private static volatile BlockingQueue<NotaVenta> queue = new LinkedBlockingDeque<>();

    public static synchronized void put(List<NotaVenta> listaNotaVenta) throws InterruptedException {
        for (NotaVenta item : listaNotaVenta) {
            put(item);
        }
    }

    public static synchronized void put(NotaVenta notaVenta) throws InterruptedException {
        queue.put(notaVenta);
    }

    public static synchronized NotaVenta take() throws InterruptedException {
        return queue.take();
    }
     */
}
