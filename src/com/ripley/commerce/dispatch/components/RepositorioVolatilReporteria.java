/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ripley.commerce.dispatch.components;

import com.ripley.commerce.dispatch.dto.MailConfiguration;
import com.ripley.commerce.dispatch.util.Constants;
import com.ripley.commerce.dispatch.util.MailUtils;
import com.ripley.commerce.dispatch.util.Utils;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author dams
 */
public class RepositorioVolatilReporteria {

    private static final Log LOGGER = LogFactory.getLog(RepositorioVolatilReporteria.class);

    private final StringBuilder stringBuilderEmailMessage;
    private final Map<String, Integer> mapStatusDispatch;
    private final Map<String, Integer> mapStatusEmails;
    private final Map<String, String> mapErrors;

    private final MailConfiguration mailConfiguration;

    private static RepositorioVolatilReporteria repositorioVolatilReporteria;

    public static void load(MailConfiguration mailConfiguration) {
        repositorioVolatilReporteria = new RepositorioVolatilReporteria(mailConfiguration);
    }

    public static RepositorioVolatilReporteria getRepositorio() {
        return repositorioVolatilReporteria;
    }

    private RepositorioVolatilReporteria(MailConfiguration mailConfiguration) {
        this.mailConfiguration = mailConfiguration;
        this.stringBuilderEmailMessage = new StringBuilder();
        this.mapStatusDispatch = new HashMap<>();
        this.mapStatusEmails = new HashMap<>();
        this.mapErrors = new HashMap<>();
    }

    public void appendMessageAtEmail(String message) {
        LOGGER.info(message);
        this.stringBuilderEmailMessage.append(message);
    }

    public void sendEmailSoporte() {
        LOGGER.debug("[METHOD] enviarEmailSoporte");

        appendMessageAtEmail("\n\n----------------------------------");
        appendMessageAtEmail("\nRESUMEN ESTADOS CUDS");
        appendMessageAtEmail("\n----------------------------------");

        prepareCounterInformation(this.mapStatusDispatch);

        appendMessageAtEmail("\n----------------------------------");
        appendMessageAtEmail("\n\n----------------------------------");
        appendMessageAtEmail("\nRESUMEN ESTADOS EMAILS");
        appendMessageAtEmail("\n----------------------------------");

        prepareCounterInformation(this.mapStatusEmails);

        appendMessageAtEmail("\n----------------------------------");
        appendMessageAtEmail("\n\n----------------------------------");
        appendMessageAtEmail("\nCUDS CON ERROR");
        appendMessageAtEmail("\n----------------------------------");

        prepareStringInformation(this.mapErrors);

        appendMessageAtEmail("\n----------------------------------");
        appendMessageAtEmail("\n\nFIN - " + Utils.getCurrentDate());

        mailConfiguration.setSubject(Constants.SUBJECT_EMAIL);
        mailConfiguration.setMensaje(this.stringBuilderEmailMessage.toString());
        MailUtils.envia(mailConfiguration);
    }

    private void prepareCounterInformation(Map<String, Integer> map) {
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>) it.next();
            appendMessageAtEmail("\n" + entry.getKey() + ": " + entry.getValue());
        }
    }

    private void prepareStringInformation(Map<String, String> map) {
        Iterator it = map.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
            appendMessageAtEmail("\n" + entry.getKey() + ": " + entry.getValue());
        }
    }

    public void agregarEstadoDespacho(String status) {
        if (this.mapStatusDispatch.containsKey(status)) {
            this.mapStatusDispatch.put(
                    status,
                    this.mapStatusDispatch.get(status) + 1
            );
        } else {
            this.mapStatusDispatch.put(status, 1);
        }
    }

    public void agregarError(String status, String cud) {
        if (this.mapErrors.containsKey(status)) {
            this.mapErrors.put(
                    status, this.mapErrors.get(status) + ", " + cud
            );
        } else {
            this.mapErrors.put(status, cud);
        }
    }

    @Deprecated
    private void mapReduceInt(Map<String, Integer> map, String status) {
        if (map.containsKey(status)) {
            map.put(status, map.get(status) + 1);
            return;
        }
        map.put(status, 1);
    }

    @Deprecated
    private void mapReduceString(Map<String, String> map, String status, String cud) {
        if (map.containsKey(status)) {
            map.put(status, map.get(status) + ", " + cud);
            return;
        }
        map.put(status, cud);
    }
}
