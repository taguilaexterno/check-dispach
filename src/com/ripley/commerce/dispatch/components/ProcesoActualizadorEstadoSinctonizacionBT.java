/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ripley.commerce.dispatch.components;

import com.ripley.commerce.dispatch.config.ApplicationParameters;
import com.ripley.commerce.dispatch.dto.NotaVenta;
import com.ripley.commerce.dispatch.service.CheckDispatchService;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author mautoro
 */
class ProcesoActualizadorEstadoSinctonizacionBT {

    private static final Log LOG = LogFactory.getLog(ProcesoEnvioCorreo.class);
    private CheckDispatchService checkDispatchService;

    public void run() {
        try {
            LOG.debug("ProcesoActualizadorEstadoSinctonizacionBT started.");
            String switchs = obtenerSwitchs();
            if (switchs != null && switchs.equals("0001")) {
                actualizarRegistrosEnTablaEstadoSincronizacionBT(
                        obtenerConsultaCargaNotaVenta(
                                ApplicationParameters.get().getInitializationQueryID()
                        ),
                        ApplicationParameters.get().getRows(),
                        ApplicationParameters.get().getInitDateDispatch()
                );
            } else {
                LOG.debug("ProcesoActualizadorEstadoSinctonizacionBT xstoreproperties CHECKDISPATCH_SWITCHS OFF.");
            }
        } catch (Exception e) {
            LOG.error("ProcesoActualizadorEstadoSinctonizacionBT closing down...", e);
            throw new RuntimeException();
        } finally {
            LOG.debug("ProcesoActualizadorEstadoSinctonizacionBT finished.");
        }
    }

    private void actualizarRegistrosEnTablaEstadoSincronizacionBT(String query, int rows, int initRange) {
        LOG.debug("[METHOD] actualizarRegistrosEnTablaEstadoSincronizacionBT");
        this.checkDispatchService.actualizarRegistrosEnTablaEstadoSincronizacionBT(query, rows, initRange);
    }

    private String obtenerConsultaCargaNotaVenta(String idConsulta) {
        LOG.debug("[METHOD] obtenerConsultaCargaNotaVenta");
        String result = this.checkDispatchService.findQuery(idConsulta);
        return result;
    }
    
    private String obtenerSwitchs(){
        LOG.debug("[METHOD] obtenerSwitchs");
        String result = null;
        try {
            result = this.checkDispatchService.findQuery("CHECKDISPATCH_SWITCHS");
        } catch (Exception e) {
            LOG.error("Error obteniendo NAME CHECKDISPATCH_SWITCHS desde JEN_PARAMETERS en BD ME: ", e);
        }
        return result;
    }

    public CheckDispatchService getCheckDispatchService() {
        return checkDispatchService;
    }

    public void setCheckDispatchService(CheckDispatchService checkDispatchService) {
        this.checkDispatchService = checkDispatchService;
    }
}
