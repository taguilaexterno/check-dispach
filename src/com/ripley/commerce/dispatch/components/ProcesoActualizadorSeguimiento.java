/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ripley.commerce.dispatch.components;

import com.ripley.commerce.dispatch.components.business.filters.FiltradorArticuloVenta;
import com.ripley.commerce.dispatch.components.business.filters.FiltradorNotaVenta;
import com.ripley.commerce.dispatch.config.ApplicationConfiguration;
import com.ripley.commerce.dispatch.dto.ArticuloVenta;
import com.ripley.commerce.dispatch.dto.Despacho;
import com.ripley.commerce.dispatch.dto.NotaVenta;
import com.ripley.commerce.dispatch.config.ApplicationParameters;
import com.ripley.commerce.dispatch.dto.TiendasRetiroEnTienda;
import com.ripley.commerce.dispatch.service.CheckDispatchService;
import com.ripley.commerce.dispatch.util.Constants;
import com.ripley.commerce.dispatch.util.DateUtils;
import com.ripley.commerce.dispatch.util.Utils;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.commons.lang.SerializationUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author dams
 */
public class ProcesoActualizadorSeguimiento {

    private static final Log LOGGER = LogFactory.getLog(ProcesoActualizadorSeguimiento.class);

    private static final int ESTADO_CUD_ACTUALIZADOS_PARCIAL = 0;
    private static final int ESTADO_CUD_TODOS_ACTUALIZADOS = 2;
    private static final int ESTADO_CUD_RT_ACTUALIZADOS_PARCIAL = 1;

    private static final String ESTADO_ARTICULO_RESERVA_CADUCADA = "Reserva Caducada";
    /* Estados terminales OK*/
    private static final String ESTADO_ARTICULO_LISTO_PARA_RETIRAR = "Listo para Retirar";

    /*Instancia de checkDispatchService est� definida en spring-config.xml*/
    private CheckDispatchService checkDispatchService;

    public void run() {
        try {
            if (ApplicationParameters.get().isEnableProcessUpdater() || (ApplicationParameters.get().isEnableProcessEmailOK() || ApplicationParameters.get().isEnableProcessEmailNOK())) {
                int rowsLimit = ApplicationParameters.get().getRows();
                int initRange = ApplicationParameters.get().getInitDateDispatch();
                int endRange = ApplicationParameters.get().getEndDateDispatch();
                int jobID = ApplicationParameters.get().getJobID();
                String initializationQueryID = ApplicationParameters.get().getInitializationQueryID();
                String wildCard = ApplicationParameters.get().getWildcard();

                String initializationQuery = obtenerConsultaCargaNotaVenta(initializationQueryID);

                // FIXME: PUAJ
                String tmpSlaExpirationTimeRT = obtenerValorParametro(ApplicationConfiguration.BD_PARAMETER_SLA_EXPIRATION_TIME_RT);
                String tmpSlaExpirationTimeST = obtenerValorParametro(ApplicationConfiguration.BD_PARAMETER_SLA_EXPIRATION_TIME_ST);
                ApplicationConfiguration.get().setExpirationTimeSLART(Integer.parseInt(tmpSlaExpirationTimeRT));
                ApplicationConfiguration.get().setExpirationTimeSLAST(Integer.parseInt(tmpSlaExpirationTimeST));

                // Se obtiene lista de notas de venta desde BD Modelo Extendido
                List<NotaVenta> listaNotaVentaInicial = obtenerListaNotaVentaPreliminar(initializationQuery, wildCard, jobID, initRange, endRange, rowsLimit);

                // Se define valor booleano al indicador de "Orden Completa" de todas las notas de venta cargadas en memoria
                //definirIndicadorOrdenCompleta(listaNotaVentaInicial);
                List<NotaVenta> listaOCIncompletas = obtenerListaFiltradaPorEstadoNoTerminal(listaNotaVentaInicial);
                List<NotaVenta> listaOCCompletas = obtenerListaFiltradaPorEstadoTerminal(listaNotaVentaInicial);

    //            if (listaOCIncompletas.size() + listaOCCompletas.size() != listaNotaVentaInicial.size()) {
    //                throw new RuntimeException("Definici�n de listas de OC completas e incompletas incorrecta");
    //            }
                if (listaNotaVentaInicial != null & !listaNotaVentaInicial.isEmpty()) {
                    RepositorioVolatilReporteria.getRepositorio().appendMessageAtEmail("\n\nTotal OCs pendientes: " + listaNotaVentaInicial.size());

                    List<NotaVenta> auxList = new ArrayList<>();

                    if (ApplicationParameters.get().isEnableProcessUpdater() && listaOCIncompletas != null && !listaOCIncompletas.isEmpty()) {
                        // Se obtiene lista de despachos desde BD Big Ticket asociado a la lista de nota de venta entregada por parametro
                        List<Despacho> listaDespachos = obtenerListaDespachos(listaOCIncompletas);

                        RepositorioVolatilReporteria.getRepositorio().appendMessageAtEmail("\nTotal CUDs en BT: " + listaDespachos.size());
                        if (!listaDespachos.isEmpty()) {
                            // Se complementa lista de notas de venta, recuperada desde BD Modelo Extendido, con datos de lista de despachos, recuperada desde BD Big Ticket
                            List<NotaVenta> listaComplementadaConBT = complementarListaNotaVentaConDatosBT(listaOCIncompletas, listaDespachos);

                            // Se actualizan articulos en BD Modelo Extendido
                            actualizarEstadoArticulos(listaComplementadaConBT);
                            List<NotaVenta> auxListActualizadaEnBT = copyAndRefreshState(listaComplementadaConBT);
                            List<NotaVenta> auxListNoTerminal = obtenerListaFiltradaPorEstadoNoTerminal(auxListActualizadaEnBT);
                            List<NotaVenta> auxListTerminal = obtenerListaFiltradaPorEstadoTerminal(auxListActualizadaEnBT);

                            // Se crea una lista que contenga Notas de Venta con todos sus articulos RT en estado final (OK o NOK)
                            List<NotaVenta> auxListRT = obtenerListaNotaVentaRTTerminal(auxListNoTerminal);

                            actualizarIndicadorEstadoCUD(auxListNoTerminal, ESTADO_CUD_ACTUALIZADOS_PARCIAL);

                            if (auxListRT != null & !auxListRT.isEmpty() & !tieneArticuloConEstadoTerminal(auxListRT)) {
                                actualizarIndicadorEstadoCUD1(auxListRT, ESTADO_CUD_RT_ACTUALIZADOS_PARCIAL);
                            }

                            actualizarIndicadorEstadoCUD2(auxListTerminal, ESTADO_CUD_TODOS_ACTUALIZADOS);


                            auxListNoTerminal = null;
                            auxList.addAll(auxListTerminal);
                        }
                    }

                    // MTP: Si se encuentra habilitado el proceso de actualizaci�n, y hubo listas para procesar: 
                    // 1. Actualiza el estado de los art�culos de venta de las listas procesadas.
                    // 2. Toma los art�culos de venta actualizados y los filtra por estado por NOK.
                    // 3. Actualiza los estados de nota de credito de los art�culos de venta filtrados.
                    // Nota: Estos m�todos no trabajan la obtenci�n de campos para NotaVenta. Solo obtienen el n�mero correlativo,
                    //       lo insertan en el objeto Art�culo de Venta (se agreg� el campo numero correlativo) y luego
                    //       s�lo se trabaja con los objetos de Art�culos de Venta.
                    if (ApplicationParameters.get().isEnableProcessUpdater()){
                        if (listaOCIncompletas != null && !listaOCIncompletas.isEmpty()) {
                            List<NotaVenta> listaAVOCIncompletasActualizada = actualizarArticulos(listaOCIncompletas);
                            for (NotaVenta nv: listaAVOCIncompletasActualizada){
                                List<ArticuloVenta> listaAVOCIncompletasActualizadaFiltrada = FiltradorArticuloVenta.filtrarLista(FiltradorArticuloVenta.FILTRO_ESTADO_FINAL_NOK, nv.getArticulos());
                                if (listaAVOCIncompletasActualizadaFiltrada != null && !listaAVOCIncompletasActualizadaFiltrada.isEmpty()){
                                    actualizarEstadoNotaDeCreditoConCUDNOK(listaAVOCIncompletasActualizadaFiltrada);
                                }
                            }
                        }
                        if (listaOCCompletas != null && !listaOCCompletas.isEmpty()){
                            List<NotaVenta> listaAVOCCompletasActualizada = actualizarArticulos(listaOCCompletas);
                            for (NotaVenta nv: listaAVOCCompletasActualizada){
                                List<ArticuloVenta> listaAVOCCompletasActualizadaFiltrada = FiltradorArticuloVenta.filtrarLista(FiltradorArticuloVenta.FILTRO_ESTADO_FINAL_NOK, nv.getArticulos());
                                if (listaAVOCCompletasActualizadaFiltrada != null && !listaAVOCCompletasActualizadaFiltrada.isEmpty()){
                                    actualizarEstadoNotaDeCreditoConCUDNOK(listaAVOCCompletasActualizadaFiltrada);
                                }
                            }
                        }
                    }
                    auxList.addAll(listaOCCompletas);

                    List<NotaVenta> listaTerminal = auxList;
                    //List<NotaVenta> listaNoTerminal = obtenerListaFiltradaPorEstadoNoTerminal(auxList);
                    //************************
                    //List<NotaVenta> listNotaVentaParaEnviarCorreo = obtenerListaOCCompletas(listaComplementadaConBT, false);
                    //listNotaVentaParaEnviarCorreo.addAll(listaOCCompletas);
                    //List<NotaVenta> listNotaVentaParaEnviarCorreo = cargarListaNotaVentaEnvioCorreo(listaComplementadaConBT);
                    List<NotaVenta> listNotaVentaParaEnviarCorreo = cargarListaNotaVentaConInfoEnvioCorreo(listaTerminal);

                    //if (listNotaVentaParaEnviarCorreo != null && !listNotaVentaParaEnviarCorreo.isEmpty()) {
                    RepositorioVolatilNotaVenta.put(listNotaVentaParaEnviarCorreo);
                    RepositorioVolatilNotaVenta.procesarEnvioCorreo = (listaTerminal != null && !listaTerminal.isEmpty());
                    //}
                }
            }
        } catch (InterruptedException e) {
            LOGGER.error("ProcesoActualizadorSeguimiento closing down...", e);
            throw new RuntimeException(e);
        }
    }

    private List<NotaVenta> obtenerListaFiltradaPorEstadoTerminal(List<NotaVenta> listaEntrada) {
        List<NotaVenta> listaSalida = new ArrayList<>();

        if (listaEntrada != null && !listaEntrada.isEmpty()) {
            listaSalida = FiltradorNotaVenta.filtrarLista(FiltradorNotaVenta.FILTRO_ESTADO_FINAL, listaEntrada);
        }
//        if (listaSalida != null && listaSalida.isEmpty()) {
//            listaSalida = null;
//        }

        return listaSalida;
    }

    private List<NotaVenta> obtenerListaFiltradaPorEstadoNoTerminal(List<NotaVenta> listaEntrada) {
        List<NotaVenta> listaSalida = new ArrayList<>();

        if (listaEntrada != null && !listaEntrada.isEmpty()) {
            listaSalida = FiltradorNotaVenta.filtrarLista(FiltradorNotaVenta.FILTRO_ESTADO_NO_TERMINAL, listaEntrada);
        }
//        if (listaSalida != null && listaSalida.isEmpty()) {
//            listaSalida = null;
//        }

        return listaSalida;
    }

    private String obtenerValorParametro(String idParam) {
        LOGGER.debug("[METHOD] obtenerValorParametro");
        String result = this.checkDispatchService.findParameter(idParam, ApplicationConfiguration.BD_PARAMETER_APPLICATION_ID);
        return result;
    }

    private String obtenerConsultaCargaNotaVenta(String idConsulta) {
        LOGGER.debug("[METHOD] obtenerConsultaCargaNotaVenta");
        String result = this.checkDispatchService.findQuery(idConsulta);
        return result;
    }

    private List<NotaVenta> obtenerListaNotaVentaPreliminar(String initializationQuery, String wildCard, int jobID, int initRange, int endRange, int rowsLimit) {
        LOGGER.debug("[METHOD] setlistNotaVentaInicial");
        List<NotaVenta> list = this.checkDispatchService.findAllOrderItems(initializationQuery, wildCard, jobID, initRange, endRange, rowsLimit);
        return list;
    }

    @Deprecated
    private List<NotaVenta> obtenerListaNotaVentaPreliminar(ApplicationParameters parameters) {
        LOGGER.debug("[METHOD] setlistNotaVentaInicial");
        List<NotaVenta> list = null;
        if (parameters.getOrderId() != null && !"".equals(parameters.getOrderId().trim())) {
            LOGGER.debug("Buscar por ordersId: " + parameters.getOrderId());
            list = this.checkDispatchService.findOrderItemsByOrderIds(parameters);
        } else {
            list = this.checkDispatchService.findAllOrderItems(parameters);
        }
        return list;
    }

    @Deprecated
    private void definirIndicadorOrdenCompleta(List<NotaVenta> listP) {
        if (listP != null & !listP.isEmpty()) {
            for (int i = 0; i < listP.size(); i++) {
                NotaVenta notaVenta = listP.get(i);
                notaVenta.setOrdenCompleta(
                        esNotaVentaUnaOrdenCompletaME(notaVenta)
                );
            }
        }
    }

    @Deprecated
    private boolean esEstadoArticuloEstadoFinal(String estadoVenta) {
        boolean flag = false;
        String auxStr = null;
        if (estadoVenta != null) {
            auxStr = estadoVenta.trim().toUpperCase();
            if (auxStr.equals(ESTADO_ARTICULO_LISTO_PARA_RETIRAR)
                    && auxStr.equals(LOGGER)
                    && auxStr.equals(LOGGER)
                    && auxStr.equals(LOGGER)) {

            }
        }
        auxStr = null;
        return flag;
    }

    @Deprecated
    private boolean esNotaVentaUnaOrdenCompletaME(NotaVenta notaVentaP) {
        boolean flag = true;
        //NotaVenta notaVenta = notaVentaP;
        //notaVentaP.setOrdenCompleta(true);
        for (int i = 0; i < notaVentaP.getArticulos().size(); i++) {
            ArticuloVenta articuloVenta = notaVentaP.getArticulos().get(i);

            //Si la orden tiene articulos con estado venta distinto a los siguientes
            //Entonces es una orden con estados donde no se enviara el correo
            if (!Constants.DISPATCH_STATE_RT_STOCK_CONFIRMADO.equals(articuloVenta.getEstadoVenta())
                    && !Constants.DISPATCH_STATE_ST_PREPARANDO_PRODUCTO.equals(articuloVenta.getEstadoVenta())
                    && !Constants.DISPATCH_STATE_ST_RECIBIDO_EN_TIENDA.equals(articuloVenta.getEstadoVenta())
                    && !Constants.DISPATCH_STATE_ST_LISTO_PARA_RETIRAR.equals(articuloVenta.getEstadoVenta())
                    && !Constants.DISPATCH_STATE_ST_EN_TRANSITO_A_TIENDA.equals(articuloVenta.getEstadoVenta())
                    && !Constants.DISPATCH_STATE_ST_PRODUCTO_RETIRADO.equals(articuloVenta.getEstadoVenta())
                    && !Constants.DISPATCH_STATE_ERROR_CORREO.equals(articuloVenta.getEstadoVenta())
                    && !Constants.DISPATCH_STATE_CUD_NO_EXISTE.equals(articuloVenta.getEstadoVenta())
                    //Si es ST y el estado venta es numerico, esta OK
                    && !(Constants.JORNADA_DESPACHO_ST.equals(articuloVenta.getTipoDespacho()) && Utils.isNumeric(articuloVenta.getEstadoVenta()))) {
                //notaVenta.setOrdenCompleta(false);
                flag = false;
                break;
            }
        }
        return flag;
    }

    private List<Despacho> obtenerListaDespachos(List<NotaVenta> listNotaVentaInicial) {
        LOGGER.debug("[METHOD] setlistCUDsBT");

        String cuds = "";
        List<Despacho> listDespachos = new ArrayList<>();
        int count = 0;
        for (int i = 0; i < listNotaVentaInicial.size(); i++) {
            NotaVenta notaVenta = listNotaVentaInicial.get(i);

            //Si la orden se encuentra sin estados para no enviar correo
            //Entonces buscar los estados de los CUDs en BigTicket
            //if (!notaVenta.isOrdenCompleta()) {
            for (int j = 0; j < notaVenta.getArticulos().size(); j++) {
                ArticuloVenta articulo = notaVenta.getArticulos().get(j);
                if (count == 0) {
                    cuds = articulo.getCodDespacho();
                    count++;
                } else {
                    cuds = cuds + "," + articulo.getCodDespacho();
                    count++;
                }

                if (count == Constants.MAX_NUMBER_QUERIES) {
                    listDespachos.addAll(this.checkDispatchService.findDispatchStatusByCUDs(cuds));
                    LOGGER.debug("Total listDespachos: " + listDespachos.size());
                    count = 0;
                    cuds = "";
                }
            }
            //}
        }
//		log.debug("cuds: " + cuds);

        //Revisamos si son menos de 500 o el sobrante que no llego a 500
        if (!"".equals(cuds)) {
            listDespachos.addAll(this.checkDispatchService.findDispatchStatusByCUDs(cuds));
        }
//		log.debug("listCUDs: " + listDespachos);
        return listDespachos;
    }

    public List<NotaVenta> complementarListaNotaVentaConDatosBT(List<NotaVenta> listaNotaVentaP, List<Despacho> listaDespachoP) {
        LOGGER.debug("[METHOD] mergeList");

        List<NotaVenta> listaComplementada = new ArrayList<>();

        for (int i = 0; i < listaNotaVentaP.size(); i++) {
            NotaVenta notaVenta = listaNotaVentaP.get(i);

            //if (notaVenta.isOrdenCompleta()) {
            for (int j = 0; j < notaVenta.getArticulos().size(); j++) {
                ArticuloVenta articuloVenta = notaVenta.getArticulos().get(j);
                for (int k = 0; k < listaDespachoP.size(); k++) {
                    Despacho despacho = listaDespachoP.get(k);
                    if (despacho.getReservaCud().equals(articuloVenta.getCodDespacho())) {
                        articuloVenta.setExisteEnBT(true);

                        articuloVenta = cargarArticuloHomologadoConBT(
                                articuloVenta,
                                despacho,
                                notaVenta.getFechaBoleta(),
                                notaVenta.getHoraBoleta()
                        );

                        notaVenta.getArticulos().set(j, articuloVenta);

                        break;
                    }
                }
            }

            notaVenta.setOrdenCompleta(checkCompletedOrderBT(notaVenta, true));
            listaComplementada.add(notaVenta);
            //}
        }

        return listaComplementada;
    }

    private List<NotaVenta> cargarListaNotaVentaConInfoEnvioCorreo(List<NotaVenta> listaComplementadaParam) {
        List<NotaVenta> listNotaVentaParaEnviarCorreo = new ArrayList<>();

        for (NotaVenta notaVenta : listaComplementadaParam) {
            //if (notaVenta.isOrdenCompleta()) {
            listNotaVentaParaEnviarCorreo.add(notaVenta);
            //}
        }

        // log.debug("listNotaVentaInicial: " + listNotaVentaInicial);
        RepositorioVolatilReporteria.getRepositorio().appendMessageAtEmail("\nTotal correos Listos para Retirar: " + listNotaVentaParaEnviarCorreo.size());

        LOGGER.debug("listNotaVentaParaEnviarCorreo: " + listNotaVentaParaEnviarCorreo);
        TiendasRetiroEnTienda tiendas = new TiendasRetiroEnTienda();

        for (int i = 0; i < listNotaVentaParaEnviarCorreo.size(); i++) {
            NotaVenta notaVenta = listNotaVentaParaEnviarCorreo.get(i);
            try {
                notaVenta.setUrlMapa(tiendas.get(Integer.parseInt(notaVenta.getCodBodega())).getUrlMapa());
                notaVenta.setHorario(tiendas.get(Integer.parseInt(notaVenta.getCodBodega())).getHorario());
                //Ejemplo: Costanera Center Avenida Andres Bello 2422 REG.METROPOLITANA LAS CONDES
                notaVenta.setDireccionDespacho(tiendas.get(Integer.parseInt(notaVenta.getCodBodega())).getDireccion());
            } catch (Exception ex) {
                LOGGER.warn("Error datos tienda RT: " + ex.getMessage() + " - notaVenta" + notaVenta);
            }

            for (int j = 0; j < notaVenta.getArticulos().size(); j++) {
                ArticuloVenta articuloVenta = notaVenta.getArticulos().get(j);
                //CAMBIOS PARA OBTENER LA IMAGEN DE ARTICULO_VENTA, TIPO_PAPEL_REGALO
                if (articuloVenta.getImagen() == null || "".equals(articuloVenta.getImagen())) {
                    //Si la imagen del producto no se encuentra en BD, buscarla a traves del rest productview
                    articuloVenta.setImagen(this.transformItemImageCorrect(checkDispatchService.findThumbnailImageItem(articuloVenta.getCodArticulo())));
                } else {
                    articuloVenta.setImagen(this.transformItemImageCorrect(articuloVenta.getImagen()));
                }
            }
        }
        return listNotaVentaParaEnviarCorreo;
    }

    private ArticuloVenta cargarArticuloHomologadoConBT(ArticuloVenta articuloVentaParam,
            Despacho despacho,
            String fechaBoleta,
            String horaBoleta) {

        ArticuloVenta articuloVenta = articuloVentaParam;
        if (despacho != null) {
            String jornadaDespacho = despacho.getJornadaDesp();
            String codigoMotivoDespacho = despacho.getCodMotivo();
            String estadoDespacho = despacho.getEstadoDespacho();
            //String estadoR = despacho.getEstadoDespacho();
            String reservaCud = despacho.getReservaCud().trim().toUpperCase();;
           // String reservaJornadaDespacho = despacho.getReservaJornadaDesp().trim().toUpperCase();;
            String reservaCodMotivo = despacho.getReservaCodMotivo().trim().toUpperCase();

            //Si la jornada en BT es distinto a ST o RT, se debe a una solucion comercial
            if (jornadaDespacho != null && !Constants.JORNADA_DESPACHO_ST.equals(jornadaDespacho) && !Constants.JORNADA_DESPACHO_RT.equals(jornadaDespacho)) {
                articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_JORNADA_DISTINTA);
            } 
            
            else if (Constants.JORNADA_DESPACHO_ST.equals(jornadaDespacho) && codigoMotivoDespacho == null && estadoDespacho.equals("3") && Constants.ESTADO_RESERVA_R4.equalsIgnoreCase(reservaCodMotivo)) {
                articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_ST);
                articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_ST_PRODUCTO_RETIRADO);
            } 
            
            else if (Constants.JORNADA_DESPACHO_RT.equals(jornadaDespacho) && codigoMotivoDespacho == null && estadoDespacho.equals("3") && Constants.ESTADO_RESERVA_R4.equalsIgnoreCase(reservaCodMotivo)) {
                articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_RT);
                articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_RT_PRODUCTO_RETIRADO);
            } 
            
            else if (Constants.JORNADA_DESPACHO_ST.equals(jornadaDespacho) && "30".equals(codigoMotivoDespacho) && "0".equals(estadoDespacho) && Constants.ESTADO_RESERVA_R4.equalsIgnoreCase(reservaCodMotivo)) {
                articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_ST);
                articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_ST_LISTO_PARA_RETIRAR);
            } 
            
            else if (Constants.JORNADA_DESPACHO_ST.equals(jornadaDespacho) && "30".equals(codigoMotivoDespacho) && "2".equals(estadoDespacho) && Constants.ESTADO_RESERVA_R4.equalsIgnoreCase(reservaCodMotivo)) {
                articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_ST);
                articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_ST_LISTO_PARA_RETIRAR);
            } 
            
            else if (Constants.JORNADA_DESPACHO_ST.equals(jornadaDespacho) && "21".equals(codigoMotivoDespacho) && "0".equals(estadoDespacho) && Constants.ESTADO_RESERVA_R4.equalsIgnoreCase(reservaCodMotivo)) {
                articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_ST);
                articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_ST_RECIBIDO_EN_TIENDA);
            } 
            
            else if (Constants.JORNADA_DESPACHO_ST.equals(jornadaDespacho) && codigoMotivoDespacho == null && "C".equals(estadoDespacho) && Constants.ESTADO_RESERVA_R4.equalsIgnoreCase(reservaCodMotivo)) {
                articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_ST);
                articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_ST_EN_TRANSITO_A_TIENDA);
            } 
            
            
            else if (Constants.JORNADA_DESPACHO_ST.equals(jornadaDespacho) && codigoMotivoDespacho == null && "1".equals(estadoDespacho) && Constants.ESTADO_RESERVA_R4.equalsIgnoreCase(reservaCodMotivo)) {
                articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_ST);
                articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_ST_PREPARANDO_PRODUCTO);
            } 
            
            
            else if (Constants.JORNADA_DESPACHO_ST.equals(jornadaDespacho) && codigoMotivoDespacho == null && "0".equals(estadoDespacho) && Constants.ESTADO_RESERVA_R4.equalsIgnoreCase(reservaCodMotivo)) {
                articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_ST);
                articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_ST_CREACION_ORDEN_COMPRA);
            } 
            
            
            else if (Constants.JORNADA_DESPACHO_RT.equals(jornadaDespacho) && codigoMotivoDespacho == null && "0".equals(estadoDespacho) && Constants.ESTADO_RESERVA_R4.equalsIgnoreCase(reservaCodMotivo)) {
                articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_RT);
                articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_RT_LISTO_PARA_RETIRAR);
            } 
            
            
            else if (Constants.JORNADA_DESPACHO_RT.equals(jornadaDespacho) && "15".equals(codigoMotivoDespacho) && "0".equals(estadoDespacho) && Constants.ESTADO_RESERVA_R4.equalsIgnoreCase(reservaCodMotivo)) {
                articuloVenta.setTipoDespacho(Constants.JORNADA_DESPACHO_RT);
                articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_RT_SIN_STOCK);
            } 
            
            
            else if (Constants.ESTADO_RESERVA_R5.equals(reservaCodMotivo)) {
                articuloVenta.setNuevoEstadoVenta(Constants.ESTADO_RESERVA_RESERVA_CADUCADA);
            } 
            
            else if (Constants.ESTADO_RESERVA_R3.equals(reservaCodMotivo)) {
                articuloVenta.setNuevoEstadoVenta(Constants.ESTADO_RESERVA_RESERVA_CANCELADA);
            } 
            
            else if (Constants.ESTADO_RESERVA_R1.equals(reservaCodMotivo)) {
                articuloVenta.setNuevoEstadoVenta(Constants.ESTADO_RESERVA_RESERVA_ONLINE);
            } 
            
            else {
                articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_SIN_CAMBIOS);
            }
            if (!FiltradorArticuloVenta.esArticuloConEstadoTerminal(articuloVenta)) {
                if (articuloVenta.getTipoDespacho() != null) {
                    boolean reservaCaduca = false;
                    if (articuloVenta.getTipoDespacho().equals(FiltradorArticuloVenta.JORNADA_RT)) {
                        reservaCaduca = reservaCaducaPorSLA(fechaBoleta, horaBoleta, ApplicationConfiguration.get().getExpirationTimeSLART());
                    } else if (articuloVenta.getTipoDespacho().equals(FiltradorArticuloVenta.JORNADA_ST)) {
                        reservaCaduca = reservaCaducaPorSLA(fechaBoleta, horaBoleta, ApplicationConfiguration.get().getExpirationTimeSLAST());
                    }
                    if (reservaCaduca) {
                        articuloVenta.setNuevoEstadoVenta(ESTADO_ARTICULO_RESERVA_CADUCADA);
                    }
                }
            }
        }
        RepositorioVolatilReporteria.getRepositorio().agregarEstadoDespacho(articuloVenta.getNuevoEstadoVenta());
        // log.debug("interpreter: " + articuloVenta.getNuevoEstadoVenta());
        return articuloVenta;
    }

    private boolean reservaCaducaPorSLA(String fechaBoleta, String horaBoleta, Integer dias) {
        String strDateMerged = DateUtils.mergeDateWithTime(fechaBoleta, horaBoleta);
        Date dateNow = new Date();
        Date mergedDate = DateUtils.stringToDate(strDateMerged);
       // mergedDate = DateUtils.addDays(mergedDate, dias);
        mergedDate = DateUtils.sumarDiasAFecha(mergedDate, dias);
        boolean esIgualOPosterior = DateUtils.isGreaterOrEquals(dateNow, mergedDate);
        return esIgualOPosterior;
    }

    @Deprecated
    private List<NotaVenta> obtenerListaOCIncompletas(List<NotaVenta> listaNotaVenta, boolean reportarError) {
        List<NotaVenta> listaOCIncompleta = new ArrayList<>();
        if (listaNotaVenta != null && !listaNotaVenta.isEmpty()) {
            for (NotaVenta item : listaNotaVenta) {
                if (!checkCompletedOrderBT(item, reportarError)) {
                    item.setOrdenCompleta(false);
                    listaOCIncompleta.add(item);
                }
            }
        }
        return listaOCIncompleta;
    }

    @Deprecated
    private List<NotaVenta> obtenerListaOCCompletas(List<NotaVenta> listaNotaVenta, boolean reportarError) {
        List<NotaVenta> listaOCCompleta = new ArrayList<>();
        if (listaNotaVenta != null && !listaNotaVenta.isEmpty()) {
            for (NotaVenta item : listaNotaVenta) {
                if (checkCompletedOrderBT(item, reportarError)) {
                    item.setOrdenCompleta(true);
                    listaOCCompleta.add(item);
                }
            }
        }
        return listaOCCompleta;
    }

    private boolean checkCompletedOrderBT(NotaVenta notaVenta, boolean reportarError) {
//		log.debug("[METHOD] checkCompletedOrder");
        boolean flag = false;

        boolean checkCompletedOrder = true;
        boolean productoRetirado = false;
        boolean itemST = false;

        //Revisando ST
        for (int i = 0; i < notaVenta.getArticulos().size(); i++) {
            ArticuloVenta articuloVenta = notaVenta.getArticulos().get(i);
            if ("ST".equals(articuloVenta.getTipoDespacho())) {
                itemST = true;
                //Si no se encuentra el CUD en BT
                if (!articuloVenta.isExisteEnBT()) {
                    checkCompletedOrder = false;
                    articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_CUD_NO_EXISTE);
                    // TODO: Reubicar definici�n de error en RepositorioVolatilReporteria
                    if (reportarError) {
                        RepositorioVolatilReporteria.getRepositorio().agregarError(articuloVenta.getNuevoEstadoVenta(), articuloVenta.getCodDespacho());
                    }

                    break;
                }
                //Si el producto ST no se encuentra Listo para Retirar, no enviar correo
                if (!Constants.DISPATCH_STATE_ST_LISTO_PARA_RETIRAR.equals(articuloVenta.getNuevoEstadoVenta())) {
                    checkCompletedOrder = false;
                    break;
                }
            }
        }

        //Revisando RT
        if (checkCompletedOrder) {
            for (int i = 0; i < notaVenta.getArticulos().size(); i++) {
                ArticuloVenta articuloVenta = notaVenta.getArticulos().get(i);
                if ("RT".equals(articuloVenta.getTipoDespacho())) {
                    //Si no se encuentra el CUD en BT
                    if (!articuloVenta.isExisteEnBT()) {
                        articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_CUD_NO_EXISTE);
                        // TODO: Reubicar definici�n de error en RepositorioVolatilReporteria
                        if (reportarError) {
                            RepositorioVolatilReporteria.getRepositorio().agregarError(articuloVenta.getNuevoEstadoVenta(), articuloVenta.getCodDespacho());
                        }
                        checkCompletedOrder = false;
                        break;
                    }
                    //Si existe ST y producto RT se encuentra Retirado, enviar correo
                    if (itemST && Constants.DISPATCH_STATE_RT_PRODUCTO_RETIRADO.equals(articuloVenta.getNuevoEstadoVenta())) {
                        continue;
                    }
                    //Si no existe ST y producto RT se encuentra Retirado, no enviar correo 
                    if (!itemST && Constants.DISPATCH_STATE_RT_PRODUCTO_RETIRADO.equals(articuloVenta.getNuevoEstadoVenta())) {
                        productoRetirado = true;
                        continue;
                    }
                    //Si producto RT se encuentra Listo para Retirar, enviar correo
                    if (!Constants.DISPATCH_STATE_RT_LISTO_PARA_RETIRAR.equals(articuloVenta.getNuevoEstadoVenta())) {
                        checkCompletedOrder = false;
                        break;
                    }
                }
            }
        }

        if (checkCompletedOrder && !productoRetirado) {
            flag = true;

        }
        return flag;
    }

    @Deprecated
    private boolean esArticuloEnEstadoFinalNOK(ArticuloVenta articuloVenta) {
        // log.debug("[METHOD] checkCompletedOrder");
        boolean flag = false;

        boolean checkCompletedOrder = false;
        boolean productoRetirado = false;
        boolean itemST = false;

        //Revisando ST
        if ("ST".equals(articuloVenta.getTipoDespacho())) {
            itemST = true;
            // Si no se encuentra el CUD en BT
            if (!articuloVenta.isExisteEnBT()) {
                checkCompletedOrder = true;
                articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_CUD_NO_EXISTE);
            } else //Si el producto ST no se encuentra Listo para Retirar, no enviar correo
            if (!Constants.DISPATCH_STATE_ST_LISTO_PARA_RETIRAR.equals(articuloVenta.getNuevoEstadoVenta())) {
                checkCompletedOrder = false;
            }
        }

        //Revisando RT
        if (checkCompletedOrder) {
            if ("RT".equals(articuloVenta.getTipoDespacho())) {
                //Si no se encuentra el CUD en BT
                if (!articuloVenta.isExisteEnBT()) {
                    articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_CUD_NO_EXISTE);
                    checkCompletedOrder = true;
                } else //Si existe ST y producto RT se encuentra Retirado, enviar correo
                if (itemST && Constants.DISPATCH_STATE_RT_PRODUCTO_RETIRADO.equals(articuloVenta.getNuevoEstadoVenta())) {
                    // Nada
                } else //Si no existe ST y producto RT se encuentra Retirado, no enviar correo 
                if (!itemST && Constants.DISPATCH_STATE_RT_PRODUCTO_RETIRADO.equals(articuloVenta.getNuevoEstadoVenta())) {
                    productoRetirado = true;
                } else //Si producto RT se encuentra Listo para Retirar, enviar correo
                if (!Constants.DISPATCH_STATE_RT_LISTO_PARA_RETIRAR.equals(articuloVenta.getNuevoEstadoVenta())) {
                    checkCompletedOrder = false;
                }
            }
        }

        if (checkCompletedOrder && !productoRetirado) {
            flag = true;

        }
        return flag;
    }

    @Deprecated
    private boolean esArticuloEnEstadoFinalOK(ArticuloVenta articuloVenta) {
        // log.debug("[METHOD] checkCompletedOrder");
        boolean flag = false;

        boolean checkCompletedOrder = true;
        boolean productoRetirado = false;
        boolean itemST = false;

        //Revisando ST
        if ("ST".equals(articuloVenta.getTipoDespacho())) {
            itemST = true;
            //Si no se encuentra el CUD en BT
            if (!articuloVenta.isExisteEnBT()) {
                checkCompletedOrder = false;
                articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_CUD_NO_EXISTE);
            } else //Si el producto ST no se encuentra Listo para Retirar, no enviar correo
            if (!Constants.DISPATCH_STATE_ST_LISTO_PARA_RETIRAR.equals(articuloVenta.getNuevoEstadoVenta())) {
                checkCompletedOrder = false;
            }
        }

        //Revisando RT
        if (checkCompletedOrder) {
            if ("RT".equals(articuloVenta.getTipoDespacho())) {
                //Si no se encuentra el CUD en BT
                if (!articuloVenta.isExisteEnBT()) {
                    articuloVenta.setNuevoEstadoVenta(Constants.DISPATCH_STATE_CUD_NO_EXISTE);
                    checkCompletedOrder = false;
                } else //Si existe ST y producto RT se encuentra Retirado, enviar correo
                if (itemST && Constants.DISPATCH_STATE_RT_PRODUCTO_RETIRADO.equals(articuloVenta.getNuevoEstadoVenta())) {
                    // Nada
                } else //Si no existe ST y producto RT se encuentra Retirado, no enviar correo 
                if (!itemST && Constants.DISPATCH_STATE_RT_PRODUCTO_RETIRADO.equals(articuloVenta.getNuevoEstadoVenta())) {
                    productoRetirado = true;
                } else //Si producto RT se encuentra Listo para Retirar, enviar correo
                if (!Constants.DISPATCH_STATE_RT_LISTO_PARA_RETIRAR.equals(articuloVenta.getNuevoEstadoVenta())) {
                    checkCompletedOrder = false;

                }
            }
        }

        if (checkCompletedOrder && !productoRetirado) {
            flag = true;

        }
        return flag;
    }

    private String transformItemImageCorrect(String urlImage) {
//		log.debug("[METHOD] transformItemImageCorrect");
        String url = "";
        if (urlImage != null && !"".equals(urlImage)) {
            if (urlImage.indexOf("ripleycl_CAT_AS/") != -1) {
                url = "http://home.ripley.cl/store/Attachment/".concat(urlImage.split("ripleycl_CAT_AS/")[1]);
            } else {
                url = "http://home.ripley.cl/store/Attachment/".concat(urlImage);
            }

        }
        return url;
    }

    private List<NotaVenta> copyAndRefreshState(List<NotaVenta> listaNotaVenta) {
        List<NotaVenta> outputList = new ArrayList<>();
        if (listaNotaVenta != null) {
            for (int i = 0; i < listaNotaVenta.size(); i++) {
                //NotaVenta notaVenta = listaNotaVenta.get(i);
                NotaVenta auxNotaVenta = (NotaVenta) SerializationUtils.clone(listaNotaVenta.get(i));
                for (int j = 0; j < auxNotaVenta.getArticulos().size(); j++) {
                    ArticuloVenta auxArticuloVenta = auxNotaVenta.getArticulos().get(j);

                    //ArticuloVenta auxArticuloVenta = (ArticuloVenta) SerializationUtils.clone(articuloVenta);
                    auxArticuloVenta.setEstadoVenta(auxArticuloVenta.getNuevoEstadoVenta());
                    auxArticuloVenta.setNuevoEstadoVenta(null);
                    //auxNotaVenta.getArticulos().add(auxArticuloVenta);
                }
                outputList.add(auxNotaVenta);
            }
        }
        return outputList;
    }

    private void actualizarEstadoArticulos(List<NotaVenta> listaNotaVenta) {
        LOGGER.debug("[METHOD] updateItems");
        if (listaNotaVenta != null) {
            for (int i = 0; i < listaNotaVenta.size(); i++) {
                NotaVenta notaVenta = listaNotaVenta.get(i);
                for (int j = 0; j < notaVenta.getArticulos().size(); j++) {
                    ArticuloVenta articuloVenta = notaVenta.getArticulos().get(j);

                    //Si el estado nuevo es distinto de null
                    //Si el estado inicial es distinto a Creacion Orden de Compra
                    //Si el estado inicial es distinto a Preparando Producto
                    //Si el estado inicial es distinto a Sin cambios de estado
                    if (articuloVenta.getNuevoEstadoVenta() != null && !articuloVenta.getNuevoEstadoVenta().equalsIgnoreCase(articuloVenta.getEstadoVenta()) //&& !Constants.DISPATCH_STATE_ST_CREACION_ORDEN_COMPRA.equals(articuloVenta.getNuevoEstadoVenta())
                            //&& !Constants.DISPATCH_STATE_ST_PREPARANDO_PRODUCTO.equals(articuloVenta.getNuevoEstadoVenta())
                            //&& !Constants.DISPATCH_STATE_SIN_CAMBIOS.equals(articuloVenta.getNuevoEstadoVenta())
                            //&& !articuloVenta.getEstadoVenta().equals(articuloVenta.getNuevoEstadoVenta())
                            ) {
                        checkDispatchService.editItem(notaVenta.getCorrelativoVenta(), articuloVenta);
                    }
                }
            }
        }
    }

    private void actualizarIndicadorEstadoCUD(List<NotaVenta> listaNotaVenta, int indicadorEstadoCUD) {
        LOGGER.debug("[METHOD] actualizarIndicadorEstadoCUD");
        if (listaNotaVenta != null) {
            for (int i = 0; i < listaNotaVenta.size(); i++) {
                NotaVenta notaVenta = listaNotaVenta.get(i);
                checkDispatchService.updateCUDState(notaVenta.getCorrelativoVenta(), indicadorEstadoCUD);
            }
        }
    }

    // A petici�n del JP.    
    private void actualizarIndicadorEstadoCUD1(List<NotaVenta> listaNotaVenta, int indicadorEstadoCUD) {
        LOGGER.debug("[METHOD] actualizarIndicadorEstadoCUD1");
        if (listaNotaVenta != null) {
            for (int i = 0; i < listaNotaVenta.size(); i++) {
                NotaVenta notaVenta = listaNotaVenta.get(i);
                checkDispatchService.updateCUDState1(notaVenta.getCorrelativoVenta(), indicadorEstadoCUD);
            }
        }
    }
    
    // A petici�n del JP.
    private void actualizarIndicadorEstadoCUD2(List<NotaVenta> listaNotaVenta, int indicadorEstadoCUD) {
        LOGGER.debug("[METHOD] actualizarIndicadorEstadoCUD2");
        if (listaNotaVenta != null) {
            for (int i = 0; i < listaNotaVenta.size(); i++) {
                NotaVenta notaVenta = listaNotaVenta.get(i);
                checkDispatchService.updateCUDState2(notaVenta.getCorrelativoVenta(), indicadorEstadoCUD);
            }
        }
    }
    
    private List<NotaVenta> actualizarArticulos(List<NotaVenta> listaNotaVenta){
        LOGGER.debug("[METHOD] actualizarEstadoArticulos");
        List<NotaVenta> list = this.checkDispatchService.actualizarArticulos(listaNotaVenta);
        return list;
    }

    @Deprecated
    public void setParameters() {
    }

    public CheckDispatchService getCheckDispatchService() {
        return checkDispatchService;
    }

    public void setCheckDispatchService(CheckDispatchService checkDispatchService) {
        this.checkDispatchService = checkDispatchService;
    }

    private List<NotaVenta> obtenerListaNotaVentaRTTerminal(List<NotaVenta> listaNotaVenta) {
        boolean esRT = false;
        boolean esST = false;
        List<NotaVenta> outputList = new ArrayList<>();
        if (listaNotaVenta != null) {
            for (int i = 0; i < listaNotaVenta.size(); i++) {
                //NotaVenta notaVenta = listaNotaVenta.get(i);
                NotaVenta auxNotaVenta = (NotaVenta) SerializationUtils.clone(listaNotaVenta.get(i));
                auxNotaVenta.setArticulos(new ArrayList<ArticuloVenta>());

                for (int j = 0; j < listaNotaVenta.get(i).getArticulos().size(); j++) {
                    ArticuloVenta auxArticuloVenta = (ArticuloVenta) SerializationUtils.clone(listaNotaVenta.get(i).getArticulos().get(j));
                    if (Constants.JORNADA_DESPACHO_RT.equalsIgnoreCase(auxArticuloVenta.getTipoDespacho())) {
                        auxNotaVenta.getArticulos().add(auxArticuloVenta);
                        esRT = true;
                    } else if (Constants.JORNADA_DESPACHO_ST.equalsIgnoreCase(auxArticuloVenta.getTipoDespacho())) {
                        esST = true;
                    }
                }
                if (auxNotaVenta.getArticulos().size() > 0) {
                    if (esRT && esST) {
                        outputList.add(auxNotaVenta);
                    }
                }
                esRT = false;
                esST = false;
            }
        }
        return outputList;
    }
    
    private List<NotaVenta> filtrarNotaVentaConArticulosTerminalesNOK(List<NotaVenta> listaEntrada) {
        List<NotaVenta> listaSalida = new ArrayList<>();
        if (listaEntrada != null && !listaEntrada.isEmpty()) {
            //FiltradorArticuloVenta.FILTRO_ESTADO_FINAL_OK
            for (NotaVenta item : listaEntrada) {
                List<ArticuloVenta> tmpArt = FiltradorArticuloVenta.filtrarLista(FiltradorArticuloVenta.FILTRO_ESTADO_FINAL_NOK, item.getArticulos());
                if (tmpArt != null && !tmpArt.isEmpty()) {
                    item.setArticulos(tmpArt);
                    listaSalida.add(item);
                }
            }
        }
        if (listaSalida.isEmpty()) {
            listaSalida = null;
        }
        return listaSalida;
    }
    
    private void actualizarEstadoNotaDeCreditoConCUDNOK(List<ArticuloVenta> listaArticulosVenta) {
//        List<NotaVenta> listaFiltradaConArticuloTerminalNOK = filtrarNotaVentaConArticulosTerminalesNOK(listaNotaVentaComplementada);
        LOGGER.debug("[METHOD] actualizarNotaDeCreditoConCUDNOK");
        if (listaArticulosVenta != null) {
            for (int i = 0; i < listaArticulosVenta.size(); i++) {
                    this.checkDispatchService.updateNCState(
                            listaArticulosVenta.get(i).getCorrelativoVenta(),
                            listaArticulosVenta.get(i).getCodDespacho(),
                            Constants.NOTA_CREDITO_ESTADO_EN_PROCESO_SOLICITUD_TC,
                            Constants.NOTA_CREDITO_ESTADO_NO_PROCESADA);
            }
        }
    }
    
    private boolean tieneArticuloConEstadoTerminal(List<NotaVenta> listaEntrada){
        for (int i=0; i < listaEntrada.size(); i++){
            for(int j=0; j < listaEntrada.get(i).getArticulos().size(); j++){
                if(!FiltradorArticuloVenta.esArticuloConEstadoTerminal(listaEntrada.get(i).getArticulos().get(j))){
                    return true;
                }
            }
        }
        return false;
    }
}
