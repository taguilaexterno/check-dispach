/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ripley.commerce.dispatch.components;

import com.ripley.commerce.dispatch.dto.MailConfiguration;
import com.ripley.commerce.dispatch.config.ApplicationParameters;
import com.ripley.commerce.dispatch.util.Constants;
import com.ripley.commerce.dispatch.util.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author dams
 */
public class OrquestadorCheckDispatch {

    private static final Log LOGGER = LogFactory.getLog(OrquestadorCheckDispatch.class);

    private static final String BEAN_PROCESO_ACTUALIZADOR_SEGUIMIENTO = "procesoActualizadorSeguimiento";
    private static final String BEAN_PROCESO_ENVIO_CORREO = "procesoEnvioCorreo";
    private static final String BEAN_PROCESO_ACTUALIZADOR_ESTADO_SINCRONIZACION_BT = "procesoActualizadorEstadoSincronizacionBT";
    private static final String BEAN_MAIL_CONFIGURATION = "mailConfiguration";

    public OrquestadorCheckDispatch() {
    }

    public void run() {
        try {
            ApplicationContext context = new ClassPathXmlApplicationContext(Constants.SPRING_CONFIG_XML);

            MailConfiguration mailConfiguration = (MailConfiguration) context.getBean(BEAN_MAIL_CONFIGURATION);
            // TODO: centralizar configuracion de la aplicacion
            RepositorioVolatilReporteria.load(mailConfiguration);

            RepositorioVolatilReporteria.getRepositorio().appendMessageAtEmail("Iniciando CheckDispatch - " + Utils.getCurrentDate() + "\n");
            LOGGER.debug("parameters: " + ApplicationParameters.get());

            /**
             * Ejecutar ProcesoActualizadorSeguimiento. Actualiza estados de
             * articulos de BD ME seg�n datos de BD BT y provee de registros a
             * SingletonList
             *
             */
            ProcesoActualizadorSeguimiento proc1 = (ProcesoActualizadorSeguimiento) context.getBean(BEAN_PROCESO_ACTUALIZADOR_SEGUIMIENTO);
            proc1.run();
            proc1 = null;
            /**
             * Ejecutar ProcesoEnvioCorreo, que se alimenta de SingletonList.
             * Envia correo de retiro a clientes y correo de reporteria a
             * Soporte del resultado del procesamiento.
             */
            ProcesoEnvioCorreo proc2 = (ProcesoEnvioCorreo) context.getBean(BEAN_PROCESO_ENVIO_CORREO);
            proc2.run();
            proc2 = null;
            
            /**
             * Ejecutar ProcesoActualizadorEstadoSinctonizacionBT.
             * Consulta a la base de datos por diferencias entre tabla nota_venta y estado_sincr_bt, e
             * inserta en estado_sinct_bt las queries no encontradas que sean RT o ST.
             */
            ProcesoActualizadorEstadoSinctonizacionBT proc3 = (ProcesoActualizadorEstadoSinctonizacionBT) context.getBean(BEAN_PROCESO_ACTUALIZADOR_ESTADO_SINCRONIZACION_BT);
            proc3.run();
            proc3 = null;
        } catch (Exception ex) {
            LOGGER.error("Error en metodo run de OrquestadorCheckDispatch", ex);
            throw ex;
        }
    }

}
