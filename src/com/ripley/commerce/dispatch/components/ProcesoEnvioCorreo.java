/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ripley.commerce.dispatch.components;

import com.ripley.commerce.dispatch.components.business.filters.FiltradorArticuloVenta;
import com.ripley.commerce.dispatch.dto.ArticuloVenta;
import com.ripley.commerce.dispatch.dto.NotaVenta;
import com.ripley.commerce.dispatch.config.ApplicationParameters;
import com.ripley.commerce.dispatch.service.CheckDispatchService;
import com.ripley.commerce.dispatch.util.Constants;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 *
 * @author dams
 */
public class ProcesoEnvioCorreo {

    private static final Log LOG = LogFactory.getLog(ProcesoEnvioCorreo.class);

    private static final int ESTADO_ENVIO_CORREO_OK = 2;
    private static final int ESTADO_ENVIO_CORREO_NOK = 1;

    private static final int TIPO_CORREO_LISTO_PARA_RETIRAR = 1;
    private static final int TIPO_CORREO_EXCEPCION = 2;

    private static final String DISPATCH_STATE_CORREO_ENVIADO = "Correo Enviado";
    private static final String DISPATCH_STATE_ERROR_CORREO = "Error Correo";

    private static final String DISPATCH_STATE_CORREO_ENVIADO_NOK = "Correo Enviado NOK";
    private static final String DISPATCH_STATE_ERROR_CORREO_NOK = "Error Correo NOK";

    private static final String TIPO_CORREO_RETIRO = "CORREO_LISTO_PARA_RETIRAR";
    private static final String TIPO_CORREO_NOK = "CORREO_NOK";

    private CheckDispatchService checkDispatchService;

    public void run() {
        try {
            if (RepositorioVolatilNotaVenta.procesarEnvioCorreo) {
                if (ApplicationParameters.get().isEnableProcessEmailOK() || ApplicationParameters.get().isEnableProcessEmailNOK()) {
                    // Se recupera lista de notas venta que se deben notificar por correo, la cual fue generada al final de ProcesoActualizadorSeguimiento
                    List<NotaVenta> listaNotaVentaNotificacion = RepositorioVolatilNotaVenta.getFullList();

                    // Se obtiene la lista de notas de ventas con informaci�n de despacho incorporada
                    List<NotaVenta> listaNotaVentaComplementada = complementarListaNotaVentaParaEnvioCorreo(listaNotaVentaNotificacion);

                    if (ApplicationParameters.get().isEnableProcessEmailOK()) {
                        List<NotaVenta> listaFiltradaConArticuloTerminalOK = filtrarNotaVentaConArticulosTerminalesOK(listaNotaVentaComplementada);
                        if(todosSusArtiulosSonTerminales(listaFiltradaConArticuloTerminalOK)){
                            // Se envia correo a los clientes notificando las notas de ventas listas para ser retiradas
                            enviarCorreoRetiroCliente(listaFiltradaConArticuloTerminalOK);
                            actualizarArticulosConEstadoEnvioCorreo(listaFiltradaConArticuloTerminalOK, TIPO_CORREO_LISTO_PARA_RETIRAR);
                            
                        }
                    }

                    if (ApplicationParameters.get().isEnableProcessEmailNOK()) {
                        List<NotaVenta> listaFiltradaConArticuloTerminalNOK = filtrarNotaVentaConArticulosTerminalesNOK(listaNotaVentaComplementada);
                        enviarCorreoExcepcionCliente(listaFiltradaConArticuloTerminalNOK);
                        enviarCorreoAvisoASoporte(listaFiltradaConArticuloTerminalNOK);
                        actualizarArticulosConEstadoEnvioCorreo(listaFiltradaConArticuloTerminalNOK, TIPO_CORREO_EXCEPCION);
                    }

                    // Se actualizan los estados de los articulos seg�n el resultado del envio de correo (exito o fracaso en el envio)
                    // TODO: agregar indicadores de estado envio correo a tabla sincr_BT
                    //actualizarArticulosConEstadoEnvioCorreo(listaNotaVentaComplementada);
                    // Se envia correo a Soporte con resumen del procesamiento de ordenes actualizadas en BD ME y notificadas a los clientes.
                }
                // MTP: Modificado por solicitud MMoya. Ahora s�lo se envia en existencia de articulos terminales NOK.
                // RepositorioVolatilReporteria.getRepositorio().sendEmailSoporte();
            }
        } catch (Exception e) {
            LOG.error("ProcesoActualizadorSeguimiento closing down...", e);
            throw new RuntimeException();
        }
    }

    private List<NotaVenta> filtrarNotaVentaConArticulosTerminalesOK(List<NotaVenta> listaEntrada) {
        List<NotaVenta> listaSalida = new ArrayList<>();
        if (listaEntrada != null && !listaEntrada.isEmpty()) {
            //FiltradorArticuloVenta.FILTRO_ESTADO_FINAL_OK
            for (NotaVenta item : listaEntrada) {
                List<ArticuloVenta> tmpArt = FiltradorArticuloVenta.filtrarLista(FiltradorArticuloVenta.FILTRO_ESTADO_FINAL_OK, item.getArticulos());
                if (tmpArt != null && !tmpArt.isEmpty()) {
                    item.setArticulos(tmpArt);
                    listaSalida.add(item);
                }
            }
        }
        if (listaSalida.isEmpty()) {
            listaSalida = null;
        }
        return listaSalida;
    }

    private List<NotaVenta> filtrarNotaVentaConArticulosTerminalesNOK(List<NotaVenta> listaEntrada) {
        List<NotaVenta> listaSalida = new ArrayList<>();
        if (listaEntrada != null && !listaEntrada.isEmpty()) {
            //FiltradorArticuloVenta.FILTRO_ESTADO_FINAL_OK
            for (NotaVenta item : listaEntrada) {
                List<ArticuloVenta> tmpArt = FiltradorArticuloVenta.filtrarLista(FiltradorArticuloVenta.FILTRO_ESTADO_FINAL_NOK, item.getArticulos());
                if (tmpArt != null && !tmpArt.isEmpty()) {
                    item.setArticulos(tmpArt);
                    listaSalida.add(item);
                }
            }
        }
        if (listaSalida.isEmpty()) {
            listaSalida = null;
        }
        return listaSalida;
    }

    private List<NotaVenta> complementarListaNotaVentaParaEnvioCorreo(List<NotaVenta> listaNotaVentaNotificacionParam) {
        LOG.debug("[METHOD] completeDataForSendingEmail");

        List<NotaVenta> listaNotaVentaComplementada = null;

        if (listaNotaVentaNotificacionParam != null) {
            List<NotaVenta> listaNotaVentaConInfoDesapacho = obtenerListaNotaVentaConInfoDesapacho(listaNotaVentaNotificacionParam);

            listaNotaVentaComplementada = complementarListaNotaVenta(listaNotaVentaNotificacionParam, listaNotaVentaConInfoDesapacho);
        }
        // log.debug("listNotaVentaParaEnviarCorreo: " + listNotaVentaParaEnviarCorreo);
        return listaNotaVentaComplementada;
    }

    private List<NotaVenta> obtenerListaNotaVentaConInfoDesapacho(List<NotaVenta> listaNotaVentaNotificacionParam) {
        List<NotaVenta> listaNotaVentaAuxiliar = new ArrayList<>();
        ApplicationParameters parameters = null;
        String ordersId = "";
        int count = 0;
        for (int i = 0; i < listaNotaVentaNotificacionParam.size(); i++) {
            NotaVenta notaVenta = listaNotaVentaNotificacionParam.get(i);
            if (count == 0) {
                ordersId = notaVenta.getCorrelativoVenta();
                count++;
            } else {
                ordersId = ordersId + "," + notaVenta.getCorrelativoVenta();
                count++;
            }

            if (count == Constants.MAX_NUMBER_QUERIES) {
                parameters = new ApplicationParameters();
                parameters.setOrderId(ordersId);
                listaNotaVentaAuxiliar.addAll(this.checkDispatchService.findOrdersByOrdersId(parameters));
                LOG.debug("Total NotaVenta: " + listaNotaVentaAuxiliar.size());
                count = 0;
                ordersId = "";
            }
        }
        //Revisamos si son menos de 500 o el sobrante que no llego a 500
        if (!"".equals(ordersId)) {
            parameters = new ApplicationParameters();
            parameters.setOrderId(ordersId);
            listaNotaVentaAuxiliar.addAll(this.checkDispatchService.findOrdersByOrdersId(parameters));
            LOG.debug("Total NotaVenta: " + listaNotaVentaAuxiliar.size());
        }
        return listaNotaVentaAuxiliar;
    }

    private List<NotaVenta> complementarListaNotaVenta(List<NotaVenta> listaNotaVentaAComplementar, List<NotaVenta> listaNotaVentaInfoDespacho) {
        List<NotaVenta> listaNotaVentaComplementada = new ArrayList<>(listaNotaVentaAComplementar);
        for (int i = 0; i < listaNotaVentaComplementada.size(); i++) {
            NotaVenta notaVentaBase = listaNotaVentaComplementada.get(i);
            for (NotaVenta notaVentaAux : listaNotaVentaInfoDespacho) {
                if (notaVentaBase.getCorrelativoVenta().equals(notaVentaAux.getCorrelativoVenta())) {
                    notaVentaBase.setEmailCliente(notaVentaAux.getEmailCliente());
                    notaVentaBase.setNombreTienda(notaVentaAux.getNombreTienda());
                    notaVentaBase.setDireccionDespacho(notaVentaBase.getDireccionDespacho() + ", " + notaVentaAux.getRegionDespacho());
                    notaVentaBase.setFechaDespacho(notaVentaAux.getFechaDespacho());
                    notaVentaBase.setNombreDespacho(notaVentaAux.getNombreDespacho());
                    notaVentaBase.setTipoDespacho(notaVentaAux.getTipoDespacho());
                    notaVentaBase.setComunaDespacho(notaVentaAux.getComunaDespacho());
                    notaVentaBase.setRegionDespacho(notaVentaAux.getRegionDespacho());
                    break;
                }
            }

        }
        return listaNotaVentaComplementada;
    }

    private void enviarCorreoRetiroCliente(List<NotaVenta> listaNotaVentaParam) {
        enviarCorreoCliente(listaNotaVentaParam,
                TIPO_CORREO_RETIRO,
                DISPATCH_STATE_CORREO_ENVIADO,
                DISPATCH_STATE_ERROR_CORREO);
    }

    private void enviarCorreoExcepcionCliente(List<NotaVenta> listaNotaVentaParam) {
        enviarCorreoCliente(listaNotaVentaParam,
                TIPO_CORREO_NOK,
                DISPATCH_STATE_CORREO_ENVIADO_NOK,
                DISPATCH_STATE_ERROR_CORREO_NOK);
    }
    
    private void enviarCorreoAvisoASoporte(List<NotaVenta> listaNotaVentaParam) {
        if (listaNotaVentaParam != null) {
            RepositorioVolatilReporteria.getRepositorio().sendEmailSoporte();
        }
    }
    
    private void enviarCorreoCliente(List<NotaVenta> listaNotaVentaParam,
            String tipoCorreo,
            String estadoCorreoEnviado,
            String estadoCorreoNoEnviado) {
        LOG.debug("[METHOD] sendEmails");

        if (listaNotaVentaParam != null) {
            for (int i = 0; i < listaNotaVentaParam.size(); i++) {
                NotaVenta notaVenta = listaNotaVentaParam.get(i);
                String emailCli = notaVenta.getEmailCliente();
                notaVenta.setEmailCliente(emailCli.replaceAll("\\s+",""));
                String requestXML = null;
                if (tipoCorreo.equals(TIPO_CORREO_RETIRO)) {
                    requestXML = this.checkDispatchService.getXMLWithDataRetiro(notaVenta);
                } else if (tipoCorreo.equals(TIPO_CORREO_NOK)) {
                    requestXML = this.checkDispatchService.getXMLWithDataNOK(notaVenta);
                }
                String response = this.checkDispatchService.sendEmail(requestXML);
                // log.debug("response: " + response);
                String status = null;
                if (response != null) {
                    status = response.split("<STATUS>")[1].split("</STATUS>")[0];
                }

                if ("0".equals(status)) {
                    for (int j = 0; j < notaVenta.getArticulos().size(); j++) {
                        ArticuloVenta articuloVenta = notaVenta.getArticulos().get(j);
                        articuloVenta.setNuevoEstadoVenta(estadoCorreoEnviado);
                        articuloVenta.setCorreoEnviado(true);
                        RepositorioVolatilReporteria.getRepositorio().agregarEstadoDespacho(articuloVenta.getNuevoEstadoVenta());
                    }
                } else {
                    for (int j = 0; j < notaVenta.getArticulos().size(); j++) {
                        ArticuloVenta articuloVenta = notaVenta.getArticulos().get(j);
                        articuloVenta.setNuevoEstadoVenta(estadoCorreoNoEnviado);
                        articuloVenta.setCorreoEnviado(false);
                        RepositorioVolatilReporteria.getRepositorio().agregarEstadoDespacho(articuloVenta.getNuevoEstadoVenta());
                        RepositorioVolatilReporteria.getRepositorio().agregarError(articuloVenta.getNuevoEstadoVenta(), articuloVenta.getCodDespacho());
                    }
                    LOG.debug("ERROR ENVIO MAIL: " + notaVenta + " - requestXML: "
                            + requestXML + " - response: " + response);
                }
            }
        }
//		log.debug("listNotaVentaParaEnviarCorreo: " + listNotaVentaParaEnviarCorreo);
    }

    private void actualizarArticulosConEstadoEnvioCorreo(List<NotaVenta> listaNotaVentaParam, int tipoCorreo) {
        LOG.debug("[METHOD] actualizarArticulosConEstadoEnvioCorreo");
        if (listaNotaVentaParam != null) {
            for (int i = 0; i < listaNotaVentaParam.size(); i++) {
                NotaVenta notaVenta = listaNotaVentaParam.get(i);
                
                // Suficiente con verficar estado de envio de correo de solo un articulo
                ArticuloVenta articuloVenta = notaVenta.getArticulos().get(0);
                //Actualizar con el estado del envio del correo (Correo Enviado/Error Correo)
                // checkDispatchService.editItem(notaVenta.getCorrelativoVenta(), articuloVenta);
                int estadoEnvioCorreo = 0;
                if (articuloVenta.isCorreoEnviado()) {
                    estadoEnvioCorreo = ESTADO_ENVIO_CORREO_OK;
                } else {
                    estadoEnvioCorreo = ESTADO_ENVIO_CORREO_NOK;
                }
                checkDispatchService.updateSendEmailState(notaVenta.getCorrelativoVenta(), estadoEnvioCorreo, tipoCorreo);

            }

        }
    }

    public CheckDispatchService getCheckDispatchService() {
        return checkDispatchService;
    }

    public void setCheckDispatchService(CheckDispatchService checkDispatchService) {
        this.checkDispatchService = checkDispatchService;
    }
    
    private boolean todosSusArtiulosSonTerminales(List<NotaVenta> listaEntrada){
        for (int i=0; i < listaEntrada.size(); i++){
            for(int j=0; j < listaEntrada.get(i).getArticulos().size(); j++){
                if(!FiltradorArticuloVenta.esArticuloConEstadoTerminal(listaEntrada.get(i).getArticulos().get(j))){
                    return false;
                }
            }
        }
        return true;
    }

}
