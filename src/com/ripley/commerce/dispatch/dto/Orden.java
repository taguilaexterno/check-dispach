package com.ripley.commerce.dispatch.dto;

import java.util.List;

public class Orden {
	
	private String correlativoVenta;
	private List<DespachoTracking> despachos;
	
	public String getCorrelativoVenta() {
		return correlativoVenta;
	}
	public void setCorrelativoVenta(String correlativoVenta) {
		this.correlativoVenta = correlativoVenta;
	}
	public List<DespachoTracking> getDespachos() {
		return despachos;
	}
	public void setDespachos(List<DespachoTracking> despachos) {
		this.despachos = despachos;
	}
	@Override
	public String toString() {
		return "Orden [correlativoVenta=" + correlativoVenta + ", despachos=" + despachos + "]";
	}
}
