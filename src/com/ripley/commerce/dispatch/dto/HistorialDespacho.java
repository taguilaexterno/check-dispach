package com.ripley.commerce.dispatch.dto;

public class HistorialDespacho {
	
	private String state;
	private String reason;
	private String date;
	private String label;
	
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	@Override
	public String toString() {
		return "HistorialDespacho [state=" + state + ", reason=" + reason + ", date=" + date + ", label=" + label + "]";
	}
}
