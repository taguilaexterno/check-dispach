package com.ripley.commerce.dispatch.dto;

import java.io.Serializable;

public class ArticuloVenta implements Serializable {

    private int correlativoVenta;
    private String codArticulo;
    private String descRipley;
    private String imagen;
    private String unidades;
    private String precio;
    private String codDespacho;
    private String tipoDespacho;
    private String estadoVenta;
    private String fechaDespacho;
    private String nuevoEstadoVenta;
    private String es_nc;
    private boolean correoEnviado;
    private boolean existeEnBT;
    private Tienda tienda;

    public int getCorrelativoVenta() {
        return correlativoVenta;
    }

    public void setCorrelativoVenta(int correlativoVenta) {
        this.correlativoVenta = correlativoVenta;
    }
    
    public String getCodArticulo() {
        return codArticulo;
    }

    public void setCodArticulo(String codArticulo) {
        this.codArticulo = codArticulo;
    }

    public String getDescRipley() {
        return descRipley;
    }

    public void setDescRipley(String descRipley) {
        this.descRipley = descRipley;
    }

    public String getImagen() {
        //Completar url con .jpg en caso que se encuentre cortada
        if (imagen != null && !"".equals(imagen)) {
            if (imagen != null && imagen.indexOf(".jpg") == -1) {
                imagen = imagen + "g";
            }
        }
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getUnidades() {
        return unidades;
    }

    public void setUnidades(String unidades) {
        this.unidades = unidades;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getCodDespacho() {
        return codDespacho;
    }

    public void setCodDespacho(String codDespacho) {
        this.codDespacho = codDespacho;
    }

    public String getTipoDespacho() {
        return tipoDespacho;
    }

    public void setTipoDespacho(String tipoDespacho) {
        this.tipoDespacho = tipoDespacho;
    }

    public String getEstadoVenta() {
        return estadoVenta;
    }

    public void setEstadoVenta(String estadoVenta) {
        this.estadoVenta = estadoVenta;
    }

    public String getFechaDespacho() {
        return fechaDespacho;
    }

    public void setFechaDespacho(String fechaDespacho) {
        this.fechaDespacho = fechaDespacho;
    }

    public String getNuevoEstadoVenta() {
        return nuevoEstadoVenta;
    }

    public void setNuevoEstadoVenta(String nuevoEstadoVenta) {
        this.nuevoEstadoVenta = nuevoEstadoVenta;
    }

    public boolean isCorreoEnviado() {
        return correoEnviado;
    }

    public void setCorreoEnviado(boolean correoEnviado) {
        this.correoEnviado = correoEnviado;
    }

    public boolean isExisteEnBT() {
        return existeEnBT;
    }

    public void setExisteEnBT(boolean existeEnBT) {
        this.existeEnBT = existeEnBT;
    }

    public Tienda getTienda() {
        return tienda;
    }

    public void setTienda(Tienda tienda) {
        this.tienda = tienda;
    }
    
    public String getEs_nc() {
        return es_nc;
    }

    public void setEs_nc(String es_nc) {
        this.es_nc = es_nc;
    }

    @Override
    public String toString() {
        return "ArticuloVenta [codArticulo=" + codArticulo 
                + ", descRipley=" + descRipley 
                + ", imagen=" + imagen
                + ", unidades=" + unidades
                + ", precio=" + precio
                + ", codDespacho=" + codDespacho
                + ", tipoDespacho=" + tipoDespacho
                + ", estadoVenta=" + estadoVenta
                + ", fechaDespacho=" + fechaDespacho
                + ", nuevoEstadoVenta=" + nuevoEstadoVenta
                + ", correoEnviado=" + correoEnviado
                + ", es_nc=" + es_nc
                + ", existeEnBT=" + existeEnBT
                + ", tienda=" + tienda + "]";
    }
}
