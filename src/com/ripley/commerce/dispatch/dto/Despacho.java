package com.ripley.commerce.dispatch.dto;

public class Despacho {

    private String cud;
    private String codMotivo;
    private String estadoDespacho;
    private String jornadaDesp;

    // FUTURE: Trasladar estos atributos a un pojo Reserva
    private String reservaCud;
    private String reservaCodMotivo;
    private String reservaJornadaDesp;
    private String descripcionMotivo;

    public String getCud() {
        return cud;
    }

    public void setCud(String cud) {
        this.cud = cud;
    }

    public String getCodMotivo() {
        return codMotivo;
    }

    public void setCodMotivo(String codMotivo) {
        this.codMotivo = codMotivo;
    }

    public String getEstadoDespacho() {
        return estadoDespacho;
    }

    public void setEstadoDespacho(String estadoDespacho) {
        this.estadoDespacho = estadoDespacho;
    }

    public String getJornadaDesp() {
        return jornadaDesp;
    }

    public void setJornadaDesp(String jornadaDesp) {
        this.jornadaDesp = jornadaDesp;
    }

    public String getReservaCud() {
        return reservaCud;
    }

    public void setReservaCud(String reservaCud) {
        this.reservaCud = reservaCud;
    }

    public String getReservaCodMotivo() {
        return reservaCodMotivo;
    }

    public void setReservaCodMotivo(String reservaCodMotivo) {
        this.reservaCodMotivo = reservaCodMotivo;
    }

    public String getReservaJornadaDesp() {
        return reservaJornadaDesp;
    }

    public void setReservaJornadaDesp(String reservaJornadaDesp) {
        this.reservaJornadaDesp = reservaJornadaDesp;
    }

    public String getDescripcionMotivo() {
        return descripcionMotivo;
    }

    public void setDescripcionMotivo(String descripcionMotivo) {
        this.descripcionMotivo = descripcionMotivo;
    }

    @Override
    public String toString() {
        return "CUD [cud=" + cud + ", codMotivo=" + codMotivo + ", estadoDespacho=" + estadoDespacho + ", jornadaDesp="
                + jornadaDesp + "]";
    }
}
