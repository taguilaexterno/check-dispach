package com.ripley.commerce.dispatch.dto;

import java.io.Serializable;
import java.util.List;

public class NotaVenta implements Serializable {

    private String correlativoVenta;
    private String emailCliente;
    private String nombreTienda;
    private String direccionDespacho;
    private String urlMapa;
    private String horario;
    private String fechaDespacho;
    private String nombreDespacho;
    private String tipoDespacho;
    private boolean ordenCompleta;
    private String codBodega;
    private String comunaDespacho;
    private String regionDespacho;

    private String fechaBoleta;
    private String horaBoleta;
    private List<ArticuloVenta> articulos;

    public String getCorrelativoVenta() {
        return correlativoVenta;
    }

    public void setCorrelativoVenta(String correlativoVenta) {
        this.correlativoVenta = correlativoVenta;
    }

    public String getEmailCliente() {
        return emailCliente;
    }

    public void setEmailCliente(String emailCliente) {
        this.emailCliente = emailCliente;
    }

    public String getNombreTienda() {
        return nombreTienda;
    }

    public void setNombreTienda(String nombreTienda) {
        this.nombreTienda = nombreTienda;
    }

    public String getDireccionDespacho() {
        return direccionDespacho;
    }

    public void setDireccionDespacho(String direccionDespacho) {
        this.direccionDespacho = direccionDespacho;
    }

    public String getUrlMapa() {
        return urlMapa;
    }

    public void setUrlMapa(String urlMapa) {
        this.urlMapa = urlMapa;
    }

    public String getHorario() {
        return horario;
    }

    public void setHorario(String horario) {
        this.horario = horario;
    }

    public String getFechaDespacho() {
        return fechaDespacho;
    }

    public void setFechaDespacho(String fechaDespacho) {
        this.fechaDespacho = fechaDespacho;
    }

    public String getNombreDespacho() {
        return nombreDespacho;
    }

    public void setNombreDespacho(String nombreDespacho) {
        this.nombreDespacho = nombreDespacho;
    }

    public String getTipoDespacho() {
        return tipoDespacho;
    }

    public void setTipoDespacho(String tipoDespacho) {
        this.tipoDespacho = tipoDespacho;
    }

    public boolean isOrdenCompleta() {
        return ordenCompleta;
    }

    public void setOrdenCompleta(boolean ordenCompleta) {
        this.ordenCompleta = ordenCompleta;
    }

    public String getCodBodega() {
        return codBodega;
    }

    public void setCodBodega(String codBodega) {
        this.codBodega = codBodega;
    }

    public List<ArticuloVenta> getArticulos() {
        return articulos;
    }

    public void setArticulos(List<ArticuloVenta> articulos) {
        this.articulos = articulos;
    }

    public String getComunaDespacho() {
        return comunaDespacho;
    }

    public void setComunaDespacho(String comunaDespacho) {
        this.comunaDespacho = comunaDespacho;
    }

    public String getRegionDespacho() {
        return regionDespacho;
    }

    public void setRegionDespacho(String regionDespacho) {
        this.regionDespacho = regionDespacho;
    }

    public String getFechaBoleta() {
        return fechaBoleta;
    }

    public void setFechaBoleta(String fechaBoleta) {
        this.fechaBoleta = fechaBoleta;
    }

    public String getHoraBoleta() {
        return horaBoleta;
    }

    public void setHoraBoleta(String horaBoleta) {
        this.horaBoleta = horaBoleta;
    }

    @Override
    public String toString() {
        return "NotaVenta [correlativoVenta=" + correlativoVenta + ", emailCliente=" + emailCliente + ", nombreTienda="
                + nombreTienda + ", direccionDespacho=" + direccionDespacho + ", urlMapa=" + urlMapa + ", horario="
                + horario + ", fechaDespacho=" + fechaDespacho + ", nombreDespacho=" + nombreDespacho
                + ", tipoDespacho=" + tipoDespacho + ", ordenCompleta=" + ordenCompleta + ", codBodega=" + codBodega
                + ", comunaDespacho=" + comunaDespacho + ", regionDespacho=" + regionDespacho + ", articulos="
                + articulos + "]";
    }
}
