package com.ripley.commerce.dispatch.dto;

import java.util.List;

public class DespachoTracking {
	
	private String dispatchId;
	private String deliveryType;
	
	private List<HistorialDespacho> historialDespachos;
	public String getDispatchId() {
		return dispatchId;
	}
	public void setDispatchId(String dispatchId) {
		this.dispatchId = dispatchId;
	}
	public String getDeliveryType() {
		return deliveryType;
	}
	public void setDeliveryType(String deliveryType) {
		this.deliveryType = deliveryType;
	}
	public List<HistorialDespacho> getHistorialDespachos() {
		return historialDespachos;
	}
	public void setHistorialDespachos(List<HistorialDespacho> historialDespachos) {
		this.historialDespachos = historialDespachos;
	}
	@Override
	public String toString() {
		return "Despacho [dispatchId=" + dispatchId + ", deliveryType=" + deliveryType + ", historialDespachos="
				+ historialDespachos + "]";
	}
}
