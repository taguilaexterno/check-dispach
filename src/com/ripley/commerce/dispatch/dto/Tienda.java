package com.ripley.commerce.dispatch.dto;

public class Tienda {
	
	private int id;
    private String nombre;
    private String urlMapa;
    private String horario;
    private String direccion;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUrlMapa() {
		return urlMapa;
	}
	public void setUrlMapa(String urlMapa) {
		this.urlMapa = urlMapa;
	}
	public String getHorario() {
		return horario;
	}
	public void setHorario(String horario) {
		this.horario = horario;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	@Override
	public String toString() {
		return "Tienda [id=" + id + ", nombre=" + nombre + ", urlMapa=" + urlMapa + ", horario=" + horario
				+ ", direccion=" + direccion + "]";
	}
}
