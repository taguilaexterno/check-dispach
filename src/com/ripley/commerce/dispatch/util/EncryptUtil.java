package com.ripley.commerce.dispatch.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class EncryptUtil {

	private EncryptUtil() {
		
	}
	
    public static String getMD5(String input) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            byte[] messageDigest = md.digest(input.getBytes());
            BigInteger number = new BigInteger(1, messageDigest);
            String hashtext = number.toString(16);

            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
    
    public static String hashToNumber(String hash,int length){
        String shortHash = hash.substring(0,length);
        char[] toNumber = shortHash.toCharArray();
        String auxNumber = "";
        for (char c : toNumber) {
            auxNumber += (int) c >95 ? (c-96)+"" : c ;
        }
        return auxNumber;
    }
}
