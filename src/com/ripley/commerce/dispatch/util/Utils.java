package com.ripley.commerce.dispatch.util;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

public class Utils {

    public static String getCurrentDate() {
        return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
    }

    public static String invokeURL(String urlString, String method,
            String request, Map<String, String> requestProperty) {
        return invokeURL(urlString, method, request, requestProperty, 0, 0);
    }

    public static String invokeURL(String urlString, String method,
            String request, Map<String, String> requestProperty,
            int silverpopConnectionTimeout, int silverpopReadTimeout) {
        String sendEncoding = "utf-8";
        HttpURLConnection urlConn = null;
        OutputStream out = null;
        InputStream in = null;

        String response = null;

        try {
            URL url = new URL(urlString);
            urlConn = (HttpURLConnection) url.openConnection();
            urlConn.setRequestMethod(method);
            urlConn.setDoOutput(true);

            if (silverpopConnectionTimeout != 0) {
                urlConn.setConnectTimeout(silverpopConnectionTimeout);
            }
            if (silverpopReadTimeout != 0) {
                urlConn.setReadTimeout(silverpopReadTimeout);
            }

            Iterator it = requestProperty.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<String, String> entry = (Map.Entry<String, String>) it.next();
                urlConn.setRequestProperty(entry.getKey(), entry.getValue());
            }

            urlConn.connect();

            if (request != null && method.equals(Constants.HTTP_METHOD_POST)) {
                out = urlConn.getOutputStream();
                out.write(request.getBytes(sendEncoding));
                out.flush();
            }

            in = urlConn.getInputStream();

            InputStreamReader inReader = new InputStreamReader(in, sendEncoding);
            StringBuffer responseBuffer = new StringBuffer();
            char[] buffer = new char[1024];
            int bytes;
            while ((bytes = inReader.read(buffer)) != -1) {
                responseBuffer.append(buffer, 0, bytes);
            }
            response = responseBuffer.toString();

        } catch (FileNotFoundException fnfe) {
            //Esto ocurre cuando el REST no trae resultados, no considerar
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.close();
                } catch (Exception e) {
                }
            }
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                }
            }
            if (urlConn != null) {
                urlConn.disconnect();
            }
        }
        return response;
    }

    public static String generateWildcards(int quantity) {
        int i = 1;
        String wildcards = "";
        while (quantity >= i) {
            if (i == 1) {
                wildcards = "?";
            } else {
                wildcards = wildcards + ", ?";
            }
            i++;
        }
        return wildcards;
    }

    public static String[] getArrayFromCommasString(String values) {
        String[] str = null;
        if (values == null) {
            str = new String[1];
            return str;
        }
        String[] arrValues = values.split(",");
        int cantidad = values.split(",").length;

        str = new String[cantidad];
        for (int i = 0; i < cantidad; i++) {
            str[i] = arrValues[i];
        }
        return str;
    }

    public static String getDateBySubtractXDay(int day) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Calendar cal = Calendar.getInstance();

        cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -day);
        return dateFormat.format(cal.getTime());
    }

    public static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
}
