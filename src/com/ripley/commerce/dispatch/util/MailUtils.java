package com.ripley.commerce.dispatch.util;

import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.ripley.commerce.dispatch.dto.MailConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class MailUtils {

    private static final Log LOGGER = LogFactory.getLog(MailUtils.class);

    /**
     *
     * Funcion que envia un correo electronico
     *
     * @param mailConfiguration: configuraciones para el envio de correos
     * @param sbCorreo: el texto del correo
     * @param subject: el asunto del correo
     * @return boolean con el resultado del envio
     *
     */
    public static boolean envia(MailConfiguration mailConfiguration) {
        try {
            Properties props = new Properties();
            props.put("mail.transport.protocol", mailConfiguration.getProtocolo());
            props.put("mail.smtp.host", mailConfiguration.getHost());
            props.put("mail.smtp.port", mailConfiguration.getPuerto());
            props.put("mail.smtp.debug", "false");

            Session mailSession = Session.getInstance(props);

            Message msg_client = new MimeMessage(mailSession);

            msg_client.setFrom(new InternetAddress(mailConfiguration.getFrom().replaceAll("\\s+","")));            

            msg_client.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailConfiguration.getTo()));

            msg_client.setSentDate(new Date());

            msg_client.setSubject(mailConfiguration.getSubject());

            msg_client.saveChanges();

            MimeMultipart mimeMultipart = new MimeMultipart();

            MimeBodyPart text = new MimeBodyPart();
            text.setDisposition("inline");
            text.setContent(mailConfiguration.getMensaje(), "text/plain");

            mimeMultipart.addBodyPart(text);

            msg_client.setContent(mimeMultipart);

            Transport.send(msg_client);
        } catch (MessagingException e) {
            LOGGER.error("Error al intentar enviar correo", e);
            return false;
        }
        return true;
    }
}
