/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ripley.commerce.dispatch.util;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DateUtils {

    private static final Log LOGGER = LogFactory.getLog(DateUtils.class);

    private static final String DEFAULT_DATETIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd";
    private static final String DEFAULT_TIME_FORMAT = "HH:mm:ss";

    public static Date stringToDate(String strDate) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(DEFAULT_DATETIME_FORMAT);
            Date date = sdf.parse(strDate);
            return date;
        } catch (Exception ex) {
            LOGGER.error(ex);
            throw new RuntimeException(ex);
        }
    }

    public static boolean isGreaterOrEquals(Date date1, Date date2) {
        boolean flag = false;
        if (date1.compareTo(date2) > 0) {
            flag = true;
        } else if (date1.compareTo(date2) == 0) {
            flag = true;
        }
        return flag;
    }

    public static String mergeDateWithTime(String strDate, String strTime) {
        String mergedString = strDate.substring(0, 10);
        mergedString = mergedString + " " + strTime.substring(11);
        return mergedString;
    }

    public static Date addDays(Date mergedDate, Integer expirationDays) {
        LocalDateTime localDateTime = mergedDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        localDateTime = localDateTime.plusDays(expirationDays);
        Date result = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
        return result;
    }
    
    public static Date sumarDiasAFecha(Date fecha, int dias){
        if (dias==0) return fecha;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); 
        calendar.add(Calendar.DAY_OF_YEAR, dias);  
        return calendar.getTime(); 
  }	
    
}
