package com.ripley.commerce.dispatch.util;

public class Constants {

    public static final String APP_VERSION = "MULTI-DESPACHO 2.8.3";
    public static final String APP_NAME = "CheckDistpatch";
    public static final String APP_DESC = "Multidespacho - Parche para tabla estado_sincr_bt OC de regalos que no fueron insertados";

    public static final String SPRING_CONFIG_XML = "spring-config.xml";

    public static final int MAX_NUMBER_QUERIES = 500;

    //El asunto del email enviado a Soporte
    public final static String SUBJECT_EMAIL = "CheckDispatch - Correos Listos para Retirar";

    //Tipo de despacho Retiro en Tienda
    public static final String JORNADA_DESPACHO_RT = "RT";
    //Tipo de despacho Site to Store
    public static final String JORNADA_DESPACHO_ST = "ST";

    public static final String JORNADA_DESPACHO_MIX_RTST = "RTST";

    //Ocurre cuando BT  cambia la jornada de despacho producto de alguna solucion comercial (S)
    public static final String DISPATCH_JORNADA_DISTINTA = "Jornada distinta en BT";

    //Estado que indica que el estado en BT es el mismo en Modelo Extendido
    public static final String DISPATCH_STATE_SIN_CAMBIOS = "Sin cambios de estado";

    //Estado que informa que el CUD No Existe en BT
    public static final String DISPATCH_STATE_CUD_NO_EXISTE = "CUD no existe en BT";

    //ST
    public static final String DISPATCH_STATE_ST_PRODUCTO_RETIRADO = "Producto Retirado";
    public static final String DISPATCH_STATE_ST_LISTO_PARA_RETIRAR = "Listo para Retirar";
    public static final String DISPATCH_STATE_ST_RECIBIDO_EN_TIENDA = "Recibido en tienda";
    public static final String DISPATCH_STATE_ST_EN_TRANSITO_A_TIENDA = "En transito a tienda";
    public static final String DISPATCH_STATE_ST_PREPARANDO_PRODUCTO = "Preparando Producto";
    public static final String DISPATCH_STATE_ST_CREACION_ORDEN_COMPRA = "Creacion Orden de Compra";

    //RT
    public static final String DISPATCH_STATE_RT_PRODUCTO_RETIRADO = "Producto Retirado";
    public static final String DISPATCH_STATE_RT_LISTO_PARA_RETIRAR = "Listo para Retirar";
    public static final String DISPATCH_STATE_RT_SIN_STOCK = "Sin Stock";
    public static final String DISPATCH_STATE_RT_STOCK_CONFIRMADO = "Stock Confirmado";

    public static final String ESTADO_RESERVA_PREPARANDO_PRODUCTO = "Preparando Producto";
    public static final String ESTADO_RESERVA_RESERVA_ONLINE = "Reserva Online";
    public static final String ESTADO_RESERVA_RESERVA_CANCELADA = "Reserva Cancelada";
    public static final String ESTADO_RESERVA_RESERVA_CADUCADA = "Reserva Caducada";
   
    
    public static final String ESTADO_RESERVA_R1 = "R1";
    public static final String ESTADO_RESERVA_R3 = "R3";
    public static final String ESTADO_RESERVA_R4 = "R4";
    public static final String ESTADO_RESERVA_R5 = "R5";

    //Estados
    public static final String DISPATCH_STATE_CORREO_ENVIADO = "Correo Enviado";
    public static final String DISPATCH_STATE_ERROR_CORREO = "Error Correo";

    public static final String HTTP_METHOD_GET = "GET";
    public static final String HTTP_METHOD_POST = "POST";
    public static final String CONTENT_TYPE_NAME = "Content-Type";
    public static final String CONTENT_TYPE_VALUE_XML = "text/xml;charset=utf-8";
    public static final String CONTENT_TYPE_VALUE_JSON = "application/json;charset=utf-8";
    
    public static final int NOTA_CREDITO_ESTADO_NO_PROCESADA = 0;
    public static final int NOTA_CREDITO_ESTADO_EN_PROCESO_SOLICITUD_TC = 1;
    public static final int NOTA_CREDITO_ESTADO_OK = 2;
    
    //Verification Code
    public static final String PREFIX_GDE = "gde";
    public static final String SUFIJO_RT = "rt";
}
